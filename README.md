# OuJeTrie
## L'application qui va facilite le tri

OuJeTrie est une application composée d'un frontend écrit en VueJs et d'un Backend écrit avec DjangoREST.

C'est une application de type Progressive Web App (PWA) donc installable sur Smartphone.

Le site est disponible à l'adresse suivant :
https://oujetrie.fr/

L'app se trouve sur l'url suivante :
https://app.oujetrie.fr/

L'application utilise la base de données https://fr.openfoodfacts.org/ pour récupérer par chaque produit le type d'emballage.

Utilisation du fond cartographique OpenStreetMap.

## Fonctionnalités de l'application Cliente

- Géolocalisation des Points d'Apport Volontaires
- Scan des emballages
- Contribution de l'utilisateur final ( incivilités, taux de remplissage,...)
- Rappel des consignes de tri
- ....

## Fonctionnalités de l'application Serveur

- Ajout de communes, communauté de communes
- Ajout de PAV
- Gestion des incivilités
- Gestion des anomalies
- Gestion des utilisateurs/permissions
- Gestion d'API REST (login/register ..)
- ..
-

## Config app Cliente :

Pour configurer l'application cliente et la faire pointer sur la backend, il faut modifier le fichier suivant :
>src\models\const.js

Et modifier la ligne suivante :

``
export const URL_BACKEND='';
``

## Développer sur la partie cliente :

```
npm install
npm run server
```

Se rendre sur l'url local suivante : http://127.0.0.1:8080

## Config app Serveur :

### Pour l'utilisation en local (sans domaine), il suffit d'exécuter les 2 dockers :
```
# Lancement de la BDD Postgresql
docker-compose -f docker-compose-postgres.yml up
# Lancement du serveur django/celery/redis
docker-compose -f docker-compose-django.yml up
```
On peut accéder à la page admin depuis cette url:  http://127.0.0.1:8000/admin

### Pour l'utilisation avec un serveur et la gestion des emails (MAILJET)

Il faut effectuer quelques modifications dans le fichier settings.py qui est dans le dossier OuJeTrie.
Il faut commencer par supprimer le commentaire pour activer l'app anymail dans la liste **INSTALLED_APPS**
puis descendre dans le fichier jusqu'à ce bloc
```
#ANYMAIL = {
#    # (exact settings here depend on your ESP...)
#    "MAILJET_API_KEY": "api_key",
#    "MAILJET_SECRET_KEY": 'secret_key',
#}

#MAILJET_API_URL = "https://api.mailjet.com/v3.1/"
#EMAIL_BACKEND = "anymail.backends.mailjet.EmailBackend"

DEFAULT_FROM_EMAIL = "noreply@oujetrie.fr"  # if you don't already have this in settings
```
Il faudra supprimer les commentaires du bloc,mettre son **api_key** et **secret_key** dans la dict **ANYMAIL** et saisir l'email utilisé dans **DEFAULT_FROM_EMAIL**

Puis modifier la variable **EMAIL_CONFIRMATION_SIGNUP** de False a True

Il faut aussi ajouter le domaine dans le fichier **docker-compose-django.yml**
```
      - "traefik.http.routers.django.rule=Host(`backend.NDD.fr`)"
...
      - "traefik.http.routers.django-secure.rule=Host(`backend.NDD.fr`)"
```
Il ne reste plus qu'à lancer les dockers:
```
# Lancement de la BDD Postgresql
docker-compose -f docker-compose-postgres.yml up
# Lancement du serveur django/celery/redis
docker-compose -f docker-compose-django.yml up
```
On peut acceder à la page admin depuis cette url:  https://backend.NDD.fr/admin

## TODO

- Refactor de l'appli cliente web (passage en Vue3 Et simplification)
- ...

## License
AGPLv3

## Contributeurs

[@ArnaudFrandon](https://gitlab.com/arnaud.frandon)

[@JulienJacquemet](https://gitlab.com/julien_jacquemet)

Toi 😃
