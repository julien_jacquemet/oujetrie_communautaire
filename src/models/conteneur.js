export default class Conteneur {
    constructor(id, level, type, type_emplacement, color, dateLevee, content,apparence, dateLastUserLevel, accesPMR) {
      this.id = id;
      this.level = level;
      this.type = type;
      this.type_emplacement = type_emplacement;
      this.color = color;
      this.content = content;
      this.apparence = apparence;
      this.dateLevee=dateLevee;
      this.dateLastUserLevel = dateLastUserLevel;
      this.accesPMR = accesPMR;
    
    }

    copy(obj) {
        obj && Object.assign(this, obj);
    }
  }
