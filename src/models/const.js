/*eslint-disable no-unused-vars*/
export const TYPE_CONTENEUR= {
    PILE: "Conteneur Pile",
    BAC_VERT: "Conteneur Vert",
    BAC_JAUNE: "Conteneur Jaune",
    BAC_BLEU: "Conteneur Bleu",
    TISSU : "Conteneur Textile",
    ORDURE_MEN: "Conteneur OM",
    POUBELLE_PETITE :'Petite Poubelle',
    COMPOSTE : "Conteneur Composte"
  };

export const URL_BACKEND='';
export const MAX_PAV_NEAR=50;
export const MAX_RADIUS_PAV_NEAR=1000*50;
export const MAX_NB_BARRECODE = 10;

export const excludeWord=['bouteille'];

export const libelleCompoLevelPav="Estimer le niveau de remplissage";
export const libelleCompoAddPav="Ajouter un Point d'Apport Volontaire";
export const libelleCompoIncivilitePav="Indiquer une incivilité";
export const libelleCompoModifyPav="Compléter un Point d'Apport Volontaire";

export const debug = false;

export const defaultListeDechet = [
  {
    "name": "Conteneur Jaune (Emballage)",
    "type": "Colonne",
    "type_emplacement": "Aérien",
    "id": 1,
    "contenant": "Conteneur",
    "contenu": "Emballage",
    "apparence": "Jaune",
    "motClesDechet":"brique, bouchon, plastique"
  },
  {
    "name": "Conteneur Bleu (Papier)",
    "type": "Colonne",
    "type_emplacement": "Aérien",
    "id": 2,
    "contenant": "Conteneur",
    "contenu": "Papier",
    "apparence": "Bleu",
    "motClesDechet":"papier, journaux, feuille"
  },
  {
    "name": "Conteneur Vert (Verre)",
    "type": "Colonne",
    "type_emplacement": "Aérien",
    "id": 3,
    "contenant": "Conteneur",
    "contenu": "Verre",
    "apparence": "Vert",
    "motClesDechet":"verre"
      
  }
];

export const modeleDechet =
{
  "name": "",
  "type": "",
  "type_emplacement": "",
  "id": -1,
  "contenant": "",
  "contenu": "",
  "apparence": ""
};

export default class Const {}