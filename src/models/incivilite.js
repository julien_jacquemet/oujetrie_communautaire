export default class Incivilite {
    constructor(id, poubelleSauvage, pavAbime, photo, comment, votingGood, votingBad, date, user) {
      this.id = id;
      this.poubelleSauvage = poubelleSauvage;
      this.pavAbime = pavAbime;
      this.photo = photo;
      this.comment = comment;
      this.votingGood = votingGood;
      this.votingBad = votingBad;
      this.date = date;
      this.user=user;
    
    }

    copy(obj) {
        obj && Object.assign(this, obj);
    }
  }
