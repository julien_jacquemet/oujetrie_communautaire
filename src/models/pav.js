export default class Pav {

    listeConteneur=[];
    listeIncivilites=[];

    constructor(id, idCom, address, commune, postalCode, photo, state, nbCont, latitude, longitude, valid, distance) {
      this.id = id;
      this.idCom = idCom;
      this.address = address;
      this.commune = commune;
      this.postalCode = postalCode;
      this.photo = photo;
      this.state = state;
      this.nbCont = nbCont;
      this.latitude=latitude;
      this.longitude=longitude;
      this.valide=valid;    
      this.distance=distance;
    }

    copy(obj) {
        obj && Object.assign(this, obj);
    }

    addConteneur(aCont)
    {
        this.listeConteneur.push(aCont);
    }

    getConteneur(indice)
    {
        return this.listeConteneur[indice];
    }

    addIncivilite(aInci)
    {
        this.listeIncivilites.push(aInci);
    }

    getLastIncivilite()
    {
        if(this.listeIncivilites.length>0)
            return this.listeIncivilites[this.listeIncivilites.length-1];
        else
            return null;
    }

    getAllConteneur()
    {
        return this.listeConteneur;
    }

    getDrivingText()
    {
        return '<button onclick="giveTrip('+this.latitude+','+this.longitude+');">Se rendre à ce PAV</button>'
    }
  }


