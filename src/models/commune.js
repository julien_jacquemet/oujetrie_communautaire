export default class Commune {    

    constructor(id, name, idComCom, contentYelloWaste, contentBlueWaste, contentGreenWaste, dateLeveePoubelle, listeActivity) {
        this.id = id;
        this.name = name;
        this.idComCom=idComCom;
        this.contentYelloWaste = contentYelloWaste;
        this.contentBlueWaste = contentBlueWaste;
        this.contentGreenWaste = contentGreenWaste;
        this.dateLeveePoubelle = dateLeveePoubelle;
        this.listeActivity = listeActivity;
      }

    copy(obj) {
        obj && Object.assign(this, obj);
    }

    

    
  }