// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// https://www.sitepoint.com/node-js-event-loop-guide/ ==> layout
import Vue from 'vue'
import vuetify from '@/plugins/vuetify'
import App from './App'
import router from './router'
import axios from 'axios'
import VueGeolocation from 'vue-browser-geolocation';
import store from "./store";
import VueCompositionApi from '@vue/composition-api'; 
import Vuex from 'vuex';
import VueSnip from 'vue-snip'
Vue.use(VueSnip)


//Pour les règles de validation
////
import { ValidationObserver, ValidationProvider, extend} from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import {required, min, max, email} from 'vee-validate/dist/rules';

//Pour les règles avec validate
Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

// Réécriture des règles
extend('required', {
  ...required,
  message: 'Le champ  {_field_} est requis'
});
extend('min', {
  ...min,
  message: 'Pas assez de caractères pour {_field_}'
});
extend('max', {
  ...max,
  message: 'Trop de caractères pour {_field_}'
});
extend('email', {
  ...email,
  message: 'Ne correspond pas à une adresse email valide'
});
///


Vue.config.productionTip = false
Vue.prototype.$http = axios 

Vue.use(VueGeolocation);
Vue.use(VueCompositionApi);
Vue.use(Vuex);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

//Pour que les composants puissent échanger des messages entre eux
export const bus = new Vue();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  vuetify,
  store,
  template: '<App/>',
  components: { App }
});
