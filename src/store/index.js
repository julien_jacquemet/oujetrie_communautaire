import Vue from "vue";
import Vuex from "vuex";

import { auth } from "./auth.module";
import CommunauteCom from "../models/communautecom";
import Commune from "../models/commune";

Vue.use(Vuex);

//Permet de stocker les données dans la session en cours
export default new Vuex.Store({
  state: {
    tokenKey: "",
    myLatitude: 0,
    myLongitude: 0,
    myCommune: "",
    myCommuneCode: "",
    myPosteCode: "",
    myStreet: "",
    communeID: "",
    comCOmID: "",
    comCom: "",
    myCommuneObj: "",
    entrepriseObj: [],
  },
  mutations: {
    setToken(state, key) {
      state.tokenKey = key;
    },

    setLatitude(state, value) {
      state.myLatitude = value;
    },

    setLongitude(state, value) {
      state.myLongitude = value;
    },

    setCommune(state, value) {
      state.myCommune = value;
    },

    setCommuneCode(state, value) {
      state.myCommuneCode = value;
    },

    setPosteCode(state, value) {
      state.myPosteCode = value;
    },

    setStreet(state, value) {
      state.myStreet = value;
    },

    setCommuneID(state, value) {
      state.communeID = value;
    },

    setComCom(state, value) {
      state.comCom = new CommunauteCom();
      state.comCom.copy(value);
    },

    setmyCommuneObj(state, value) {
      state.myCommuneObj = new Commune();
      state.myCommuneObj.copy(value);
    },

    setEntreprise(state, value) {
      state.entrepriseObj = value;
    },
  },
  actions: {},
  modules: {
    auth,
  },
});
