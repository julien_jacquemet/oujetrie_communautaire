import axios from "axios";
import { reactive  } from '@vue/composition-api'; 
import store from '../store'
import { URL_BACKEND} from '../models/const';

//Permet de faire une requête de connexion
export const doLogin =async (username, password) => {
  
  const state = reactive({    
    key: '',
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });
 
  try{
    const response = await axios.post(URL_BACKEND+'/rest-auth/login/' , {
        username: username,
        password: password,
      
      }, {
        emulateJSON: true 
      });
		

    state.response = await response['data'] 
    store.commit("setToken", response["data"]["key"]);
    state.load=true;
    state.key=response["data"]["key"]
    state.codeStatus='';

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
      
    state.errored=true;
  }


    return state
}

//Requête pour la création d'un PAV, il faut que le user soit identifié
export const createPav =async (bodyFormData) => {
  
  const state = reactive({    
    idCreatePav: '',
    errored: false,
    load: true,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });

  const user = JSON.parse(localStorage.getItem('user'));  

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/set/pavs/' ,bodyFormData,
      {           
        emulateJSON: false,   
        headers: {                
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'Authorization': 'Token '+user.token,                
          },  
      });      
      
    state.response = await response['data'] 
    //console.log(response['data'] )
    state.idCreatePav=response['data']['id'];
    state.codeStatus='';
    state.load=false;

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
      
    state.errored=true;
  }
   
    return state
}

//Requête pour la création d'un conteneur, il faut que le user soit identifié
export const createConteneur =async (bodyFormData) => {
  
  const state = reactive({    
    idCont: '',
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });

  const user = JSON.parse(localStorage.getItem('user'));

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/set/cont/' ,bodyFormData,
      {           
        emulateJSON: false,   
        headers: {                
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'Authorization': 'Token '+user.token,                
          }, 
      });      
      
    state.response = await response['data'] 
    state.idCont=response['data']['id'];
    state.load=true;
    state.codeStatus='';

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
      
    state.errored=true;
  }
   
    return state
}

//Requête pour la création d'une incivilité, il faut que le user soit identifié
export const createIncivilite =async (bodyFormData) => {
  
  const state = reactive({    
    idIncivilite: '',
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });

  const user = JSON.parse(localStorage.getItem('user'));

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/add/inci/ ' ,bodyFormData,
      {           
        emulateJSON: false,   
        headers: {                
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'Authorization': 'Token '+user.token,                
          }, 
      });      
      
    state.response = await response['data'] 
    state.idIncivilite=response['data']['id'];
    state.load=true;
    state.codeStatus='';

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
  }
   
    return state
}

//Requête pour l'upload de photo, il faut que le user soit identifié
export const uploadFileInPav =async (bodyFormData) => {
  
  const state = reactive({    
    idCont: '',
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });
  
  const user = JSON.parse(localStorage.getItem('user'));
  //console.log(user)

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/upload/' ,bodyFormData,
      {      
        headers: {                
          "Content-Type": "multipart/form-data",
          'Authorization': 'Token '+user.token,                
          }, 
      });      
      
    state.response = await response['data'] 
    state.status=response['status']
    state.codeStatus='';

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
      
    state.errored=true;
  }
   
    return state
}

//Requête pour mettre voter sur une incivilité, il faut que le user soit identifié
export const updateVotingIncivilities =async (bodyFormData) => {
  
  const state = reactive({     
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });

  const user = JSON.parse(localStorage.getItem('user'));

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/vote/inci/' ,bodyFormData,
      {           
        emulateJSON: false,   
        headers: {                
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'Authorization': 'Token '+user.token,                
          },
      });      
      
    state.response = await response['data'] 
    state.idIncivilite=response['data']['id'];
    state.load=true;
    state.codeStatus='';

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
      
    state.errored=true;
  }
   
    return state
}

//Requête pour mettre à jour le niveau de remplissage d'un conteneur, il faut que le user soit identifié
export const updateLevelConteneur =async (bodyFormData) => {
  
  const state = reactive({     
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });

  const user = JSON.parse(localStorage.getItem('user'));

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/update/fillinglevel/' ,bodyFormData,
      {           
        emulateJSON: false,   
        headers: {                
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'Authorization': 'Token '+user.token,                
          },  
      });      
      
    state.response = await response['data'] 
    state.idIncivilite=response['data']['id'];
    state.load=true;
    state.codeStatus='';

  }
  catch(error)
  {   
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }

    state.errored=true;
  }
   
    return state
}

//Requête pour la création d'une incivilité, il faut que le user soit identifié
export const createAnomalie =async (bodyFormData) => {
  
  const state = reactive({    
    idAnomalie: '',
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });

  const user = JSON.parse(localStorage.getItem('user'));

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/add/anomalie/' ,bodyFormData,
      {           
        emulateJSON: false,   
        headers: {                
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'Authorization': 'Token '+user.token,                
          }, 
      });      
      
    state.response = await response['data'] 
    state.idAnomalie=response['data']['id'];
    state.load=true;
    state.codeStatus='';

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
  }
   
    return state
}



export const createDemandeAjoutCom =async (bodyFormData) => {
  
  const state = reactive({
    errored: false,
    load: false,
    response: null,
    errorResponse:'',
    codeStatus:'-1'
  });

  try
  {
    const response = await axios.post(URL_BACKEND+'/api/v1/add/demandeajoutcom/ ' ,bodyFormData,
      {           
        emulateJSON: false,   
        headers: {                
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          }, 
      });      
      
    state.response = await response['data'] ;
    state.load=true;
    state.codeStatus='';

  }
  catch(error)
  {
    if(error.response!=null)
    {
      state.codeStatus=error.response.status; 
    
      if (state.codeStatus=='401')
        state.errorResponse=error.response.data.detail;
      else if (state.codeStatus=='400')
        state.errorResponse=error.response.data.error;
    }
    else
    {
      state.codeStatus="-1";
      state.errorResponse="Réseau non disponible, demande archivée";
    }
  }
   
    return state
}

