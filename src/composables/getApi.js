import axios from "axios";
import { onMounted, reactive  } from '@vue/composition-api';
import { latLng } from 'leaflet';
import store from '../store'
import Pav from "../models/pav";
import Incivilite from "../models/incivilite";
import Conteneur from "../models/conteneur";
import CommunauteCom from "../models/communautecom";
import Commune from "../models/commune";
import Entreprise from "../models/entreprise";
import { URL_BACKEND, MAX_PAV_NEAR, MAX_RADIUS_PAV_NEAR, defaultListeDechet } from '../models/const';

//POur obtenir tous les PAV, uniquement en mode test
export const getAllPav = () => {
  const state = reactive({
    data: null,
    errored: false,
    load: true,
    response: null,
  });
  
  onMounted(async () => { 

    try {
      const response = await axios.get(URL_BACKEND+'/api/v1/pavs/' ,
        {
          emulateJSON: true,           
        }); 

      state.response = await response['data'] 
      //console.log(state.response)
    
      let locations=[];	  
      let position=[];

      // juste pour eviter de traiter la pagination
      if (response.data.count > 10)
        response.data.count=10


      for (let i = 0; i < response.data.count; i++) {
      //console.log(response.data.results); 

        let aPav =new Pav(response.data.results.features[i].id,
        response.data.results.features[i].properties.commune.id,
        response.data.results.features[i].properties.address,
        response.data.results.features[i].properties.commune.name,
        response.data.results.features[i].properties.commune.postalCode,
        URL_BACKEND+response.data.results.features[i].properties.photo,
        response.data.results.features[i].properties.commune.state,
        response.data.results.features[i].properties.conteneurs.length,
        response.data.results.features[i].geometry.coordinates[1],
        response.data.results.features[i].geometry.coordinates[0],
        response.data.results.features[i].properties.valide,
        response.data.results.features[i].properties.distance
        )

      let conteneurs=response.data.results.features[i].properties.conteneurs;
      let nb_conteneur=conteneurs.length
      let incivilities=response.data.results.features[i].properties.incivilite;
      let nb_Incivilites=incivilities.length;
        
     
      for (let z = 0; z < nb_conteneur; z++)
      {
        var aCont = new Conteneur(conteneurs[z]['id'], conteneurs[z]['fillingLevel']['level'], conteneurs[z]['type'], conteneurs[z]['type_emplacement'], conteneurs[z]['color'], conteneurs[z]['dateLeveePassageCont'], conteneurs[z]['content'], conteneurs[z]['apparence']);
        aPav.addConteneur(aCont);
      }

      for (let z = 0; z < nb_Incivilites; z++)
      {
        var aIncivi=new Incivilite(incivilities[z]['id'],incivilities[z]['poubelleSauvage'], incivilities[z]['pavAbime'],
        incivilities[z]['photo'], incivilities[z]['comment'],incivilities[z]['votingGood'],incivilities[z]['votingBad'], incivilities[z]['date'])    
        aPav.addIncivilite(aIncivi);
      }


        let photo=URL_BACKEND+response.data.results.features[i].properties.photo;
        let txtDriveto='<button onclick="giveTrip('+position[0]+','+position[1]+');">Se rendre à ce PAV</button>'
       
        locations.push({
          id: i,
          latlng: latLng(parseFloat(aPav.latitude), parseFloat(aPav.longitude)),
          photoImg: photo,
          txtDriveto: txtDriveto,
          pavInfo:aPav
        })
      }
       
      state.data =locations;   
      state.load=true; 
    }
    catch(error)
    {
      state.errored=true;
    }
   	
  })
  
  return state;
};

//Pour obtenir l'ensemble des PAV en fonction d'une longitude/latitude
export const getNearPav =async (latitude, longitude, limit, radius) => {
  
  const state = reactive({    
    pav: [],
    errored: false,
    load: false,
    response: null,
  });

  if (latitude==null || latitude==-1)
    latitude=store.state.myLatitude

  if (longitude==null || longitude==-1)
    longitude=store.state.myLongitude
  
  //on limite le nombre à 50
  if (limit==null || limit==-1)
    limit=MAX_PAV_NEAR;

  //on limite à un rayon de 10 km
  if (radius==null || radius==-1)
    radius=MAX_RADIUS_PAV_NEAR;


  try{
    const response = await axios.get(URL_BACKEND+'/api/v1/pavsn/',{params: {
      position: 'POINT('+longitude+' '+latitude+')',
      limit:limit,
      format:'json'
      } 
    } ,{
      emulateJSON: true,
      // <-- This was missing
      });

    state.response = await response['data'] 
    //console.log(response['data'])
    
    //liste les PAV et leur conteneurs
    for (let i = 0; i < response.data.count; i++) {
     
      let aPav =new Pav(response.data.results.features[i].id,
        response.data.results.features[i].properties.commune.id,
        response.data.results.features[i].properties.address,
        response.data.results.features[i].properties.commune.name,
        response.data.results.features[i].properties.commune.postalCode,
        URL_BACKEND+response.data.results.features[i].properties.photo,
        response.data.results.features[i].properties.commune.state,
        response.data.results.features[i].properties.conteneurs.length,
        response.data.results.features[i].geometry.coordinates[1],
        response.data.results.features[i].geometry.coordinates[0],
        response.data.results.features[i].properties.valide,
        response.data.results.features[i].properties.distance
        )

      //console.log(response.data.results.features[i].properties.valid)

      let conteneurs=response.data.results.features[i].properties.conteneurs;
      let nb_conteneur=conteneurs.length
      let incivilities=response.data.results.features[i].properties.incivilite;
      let nb_Incivilites=incivilities.length;
        
     
      for (let z = 0; z < nb_conteneur; z++)
      {
        var aCont = new Conteneur(conteneurs[z]['id'], conteneurs[z]['fillingLevel']['level'], conteneurs[z]['type'], conteneurs[z]['type_emplacement'],
          conteneurs[z]['color'], conteneurs[z]['dateLeveePassageCont'], conteneurs[z]['content'], conteneurs[z]['apparence'], conteneurs[z]['fillingLevel']['lastupdate'],
          conteneurs[z]['accesPMR']);
        aPav.addConteneur(aCont);
      }

      for (let z = 0; z < nb_Incivilites; z++)
      {
        var aIncivi=new Incivilite(incivilities[z]['id'],incivilities[z]['poubelleSauvage'], incivilities[z]['pavAbime'],
        incivilities[z]['photo'], incivilities[z]['comment'],incivilities[z]['votingGood'],incivilities[z]['votingBad'], incivilities[z]['date'], incivilities[z]['user'])    
        aPav.addIncivilite(aIncivi);
      }

        
      state.pav.push(aPav)

      //console.log(state.pav)
        
      }

    state.load=true;   
  } 
  catch (error)
  {
    console.log(error)
    state.errored=true;
  }

    return state
}

//Pour obtenir le packaging d'un produit
export const searchPackaging =async (barrecode) => {
  
  const state = reactive({    
    packaging: '',
    errored: false,
    load: false,
    response: null,
  });

  try{
 
    //const response = await axios.get('https://world.openfoodfacts.org/api/v0/product/'+barrecode+'.json');
    const response = await axios.get(URL_BACKEND+'/api/v1/product/'+barrecode);

    state.response =  await response['data'] 
	  //console.log(state.response)
    //console.log(response["data"]);
    state.load=true;
    //state.packaging=response["data"]["product"]['packaging'];
    state.packaging=response['data']['packaging']
  }
  catch(error)
  {
    state.errored=true;
  }

    return state
}

//Pour récupérer l'id d'une commune en fonction de son nom
export const getCommuneId = async(nameCommune) => {
  
  const state = reactive({
    communeID: '',
    errored: false,
    load: true,
    data: null,
  });

  if (nameCommune==null || nameCommune=='')
  {
    state.errored=true;
    state.load=false;
    return state;
  }  

  try
  {
    const response = await axios.get(URL_BACKEND+'/api/v1/commune/search?format=json&search='+nameCommune,
        {
          emulateJSON: true,     
        }); 
      //users.value = await response['data']['features'][0]['properties']['citycode'];
    state.data = await response['data'] 
    //console.log(response['data'] )

    state.CommuneID = response['data'][0]['id'];
    store.commit("setCommuneID",state.CommuneID)
    //console.log(store.state.communeID)
    
  }
  catch(error)
  {
    state.errored=true;
  }

  return state;
};

//Pour récupérer une commune et son ID en fonction de la latitude/longitude
export const getCommuneFromLatLong = async() => {
  
  const state = reactive({
    myCommune: '',
    myCommuneCode: '',
    myStreet: '',
    myPosteCode: '',
    errored: false,
    load: true,
    data: null,
  });

  try
  {
    const response = await axios.get(URL_BACKEND+'/api/v1/commune/search?format=json&position=POINT('+store.state.myLongitude+' '+store.state.myLatitude+')' ,
        {
          emulateJSON: true,     
        }); 
   
    state.data = await response['data'] 

    //console.log(response['data'] )
       
    if (response['data'].length > 0 )
    {
      //Récupère la liste des déchets d'une comcom, chaque déchet correspond à un type (emballage, papier) dans un Contenant (bac, conteneur) ....
      //se réfer à al défintion en backend d'un déchet
      let listeDechetFromBackend = response['data'][0]['communauteCommune']['dechet']
      //console.log(listeDechetFromBackend);
      
      //Si la liste est vide c'est qu'il n'a pas été défini de liste de déchet
      //Dans ce cas on utilise la liste de constante
      if (listeDechetFromBackend.length == 0)
        listeDechetFromBackend = defaultListeDechet

      let comComObj=new CommunauteCom(response['data'][0]['communauteCommune']['id'], response['data'][0]['communauteCommune']['name'], response['data'][0]['communauteCommune']['contentYelloWaste'], response['data'][0]['communauteCommune']['contentBlueWaste'], response['data'][0]['communauteCommune']['contentGreenWaste'], response['data'][0]['communauteCommune']['desc'], response['data'][0]['communauteCommune']['listeActivity'], listeDechetFromBackend)
      let communeObj= new Commune(response['data'][0]['id'],response['data'][0]['name'],response['data'][0]['communauteCommune']['id'],'','','',response['data'][0]['dateLeveePoubelle'],'')
      
      state.myCommune = response['data'][0]['name']
      store.commit("setCommune", response['data'][0]['name'])
      store.commit("setCommuneCode", response['data'][0]['postalCode'])
      store.commit('setComCom',comComObj)
      store.commit('setmyCommuneObj',communeObj)
      store.commit("setCommuneID",response['data'][0]['id'])
    }
    //console.log(store.state.myCommune)
    //console.log(store.state.myCommuneCode)
    //console.log(store.state.myStreet)
    //console.log(store.state.myPosteCode) 
  }
  catch(error)
  {
    console.log(error)
    state.errored=true;
  }

  return state;
};

//Pour récupérer une commune en fonction de la latitude/longitude via l'API GOUV
export const getCommuneFromLatLongFromGouv = async() => {
  
  const state = reactive({
    myCommune: '',
    myCommuneCode: '',
    myStreet: '',
    myPosteCode: '',
    errored: false,
    load: true,
    data: null,
  });

  try
  { 
    const response = await axios.get('https://api-adresse.data.gouv.fr/reverse/?lon='+store.state.myLongitude+'&lat='+store.state.myLatitude ,
        {
          emulateJSON: true,     
        }); 
   
    state.data = await response['data'] 

    if(response['data']['features'].length>0)
    {
      state.myCommune = response['data']['features'][0]['properties']['city'];
      state.myCommuneCode = response['data']['features'][0]['properties']['citycode']
      state.myStreet = response['data']['features'][0]['properties']['name']
      state.myPosteCode = response['data']['features'][0]['properties']['postcode']
        
      store.commit("setCommune", response['data']['features'][0]['properties']['city'])
      store.commit("setCommuneCode",response['data']['features'][0]['properties']['citycode'])
      store.commit("setPosteCode",response['data']['features'][0]['properties']['postcode'])
      store.commit("setStreet",response['data']['features'][0]['properties']['name'])
    }
    
    //console.log(response['data'])
    
  }
  catch(error)
  {
    console.log(error)
    state.errored=true;
  }

  return state;
};

//Pour récupérer une commune en fonction de la latitude/longitude via l'API OpenStreetMap
export const getCommuneFromLatLongFromOpenStreetMap = async() => {
  
  const state = reactive({
    myCommune: '',
    myCommuneCode: '',
    myStreet: '',
    myPosteCode: '',
    errored: false,
    load: true,
    data: null,
  });

  try
  { 
    const response = await axios.get('https://nominatim.openstreetmap.org/reverse?format=json&zoom=18&addressdetails=1&lon='+store.state.myLongitude+'&lat='+store.state.myLatitude ,
        {
          emulateJSON: true,     
        }); 
   
    state.data = await response['data'] 

    if( "place_id" in response['data'])
    {
      state.myCommune = response['data']['address']['village'];
      state.myCommuneCode = response['data']['address']['postcode']
      state.myStreet = response['data']['address']['road']
      state.myPosteCode = response['data']['address']['postcode']
        
      store.commit("setCommune", state.myCommune)
      store.commit("setCommuneCode",state.myCommuneCode)
      store.commit("setPosteCode",state.myPosteCode)
      store.commit("setStreet",state.myStreet)
    }
    
    //console.log(response['data'])
    
  }
  catch(error)
  {
    console.log(error)
    state.errored=true;
  }

  return state;
};

//Pour obtenir les latitude/longitude d'une ville donnée en paramètre
export const getLocationFromCity = async(cityName) => {
  
  const state = reactive({
    latitude:-1,
    longitude:-1,
    errored: false,
    load: true,
    data: null,
  });

  try
  {
    const response = await axios.get(URL_BACKEND+'api/v1/commune/search?search='+cityName,
        {
          emulateJSON: true,     
        }); 
   
    state.data = await response['data'] 
    state.longitude=response.data[0].position.coordinates[0];
    state.latitude=response.data[0].position.coordinates[1]

    //console.log(response.data[0].position.coordinates[0])

    //concatener le json
    state.json=response['data'] 

    
  }
  catch(error)
  {
    state.errored=true;
  }

  return state;
};

export const getCityFromName = async(cityName) => {
  
  const state = reactive({   
    errored: false,
    load: true,
    data: null,
  });

  try
  {
    const response = await axios.get(URL_BACKEND+'/api/v1/commune/search?search='+cityName,
        {
          emulateJSON: true,     
        }); 
   
    await response['data'];

    //console.log(response.data.length);

    let dataResult=[];
    for (let i = 0; i < response.data.length; i++) {  
      dataResult.push({
        name : response.data[i].name+" "+ response.data[i].postalCode,
        city : response.data[i].name, 
        latitude : response.data[i].position.coordinates[1],
        longitude : response.data[i].position.coordinates[0]
      });  
    }
    state.data=dataResult; 
    
  }
  catch(error)
  {
    state.errored=true;
  }

  return state;
};

export const getEntreprise = async() => {
  
  const state = reactive({   
    errored: false,
    load: true,
    data: null,
  });

  try
  {
    const response = await axios.get(URL_BACKEND+'/api/v1/entreprise/',
        {
          emulateJSON: true,     
        }); 
   
    await response['data'];
   
    let listeEntreprise=[];
    for (let i = 0; i < response.data.results.length; i++) {       
      let entrepriseObj = new Entreprise(response['data']['results'][i]['id'], response['data']['results'][i]['name'], response['data']['results'][i]['desc'],
        response['data']['results'][i]['listeActivity'], response['data']['results'][i]['d1_type'],
        response['data']['results'][i]['d1_contenu']);    
      listeEntreprise.push(entrepriseObj);  
    }
    state.data = listeEntreprise;
    
  }
  catch(error)
  {
    state.errored=true;
  }

  return state;
};

export const getConfAppCitoyen = async(idCom) => {
  
  const state = reactive({   
    errored: false,
    load: true,
    data: null,
  });

  try
  {
    const response = await axios.get(URL_BACKEND+'/api/v1/configappcitoyen/',
        {
          emulateJSON: true,     
        }); 
   
    await response['data'];
   
    let menuDisplay=[];
    for (let i = 0; i < response.data.results.length; i++) {
      if(response['data']['results'][i]['communauteCommune']['id']==idCom)
      {
        if (response['data']['results'][i]['affichage']==false)
        {
          menuDisplay.push({"menu":response['data']['results'][i]['nomMenu']});  
        }
      }  
    }
    state.data = menuDisplay;
    
  }
  catch(error)
  {
    state.errored=true;
  }

  return state;
};

export const getNbDemandeAjoutCom= async() => {
  
  const state = reactive({   
    errored: false,
    load: true,
    data: null,
  });

  try
  {
    const response = await axios.get(URL_BACKEND+'/api/v1/demandeajoutcom/',
        {
          emulateJSON: true,     
        }); 
   
    await response['data'];
    let nbDemandeAjout = 0;
    
    for (let i = 0; i < response.data.length; i++) {
      if(response['data'][i]['communauteCommune']['id']==store.state.comCom.id)
      {
        nbDemandeAjout=nbDemandeAjout+1
      }  
    }
  
    state.data = nbDemandeAjout; 
    
  }
  catch(error)
  {
    state.errored=true;
  }

  return state;
};

