import { reactive } from "@vue/composition-api";
import store from "../store";

export function formatDateCustom(date) {
  var days = [
    "lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
    "Dimanche",
  ];

  var months = [
    "Janvier",
    "Fevrier",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Decembre",
  ];
  //console.log(date)
  let actualDate = new Date(date);
  let toReturn =
    "le " +
    days[new Date(date).getDay()] +
    "  " +
    new Date(date).getDate() +
    " " +
    months[new Date(date).getMonth()] +
    " " +
    new Date(date).getFullYear();
  toReturn +=
    " à " +
    actualDate.toLocaleTimeString(navigator.language, {
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });

  return toReturn;
}

export async function dataURLtoFile(dataurl, filename) {
  var arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
}

export const getPosition = async () => {
  const state = reactive({
    coordinates: null,
    errored: false,
    load: false,
    response: null,
  });

  try {
    const coordinates = await this.$getLocation({
      enableHighAccuracy: true,
    });

    state.coordinates = coordinates;
    store.commit("setLatitude", coordinates.lat);
    store.commit("setLongitude", coordinates.lng);
    state.load = true;
  } catch (error) {
    state.errored = true;
  }

  return state;
};
