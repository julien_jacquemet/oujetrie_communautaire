import axios from 'axios';
import { URL_BACKEND } from '../models/const';
//https://bezkoder.com/jwt-vue-vuex-authentication/

const API_URL = URL_BACKEND+'/auth/';

class AuthService {

  //Logguer un utilisateur
  login(user) {
    return axios
      .post(API_URL + 'login/', {
        username: user.username,
        password: user.password,
        platform:'Android'
      }, {
        emulateJSON: true 
      } )
      .then(response => {
        
        //ajout de la date
        response.data.date=new Date();

        if (response.data.token) {
          localStorage.setItem('user', JSON.stringify(response.data));  
        }

        return response.data;
      });
  }

  //logout d'un utilisateur
  logout() {
    return axios
      .post(API_URL + 'logout/',{},
      {  
        headers: {
          'Authorization': 'Token '+JSON.parse(localStorage.getItem('user')).token               
          },
          emulateJSON: true,  
      })
      .then(response => {
        localStorage.removeItem('user');
        return response.data
      },      
      error =>{
        localStorage.removeItem('user');
        return error;
      });
 
  }

  //Enregistrement d'un utilisateur
  async register(user) {
    return axios.post(API_URL + 'registration/', {
      username: user.username,
      email: user.email,
      password1: user.password,
      password2: user.password,
    }, {
        emulateJSON: true  
      })
    .catch ( function (error){

      var reasonError='Erreur inconnue';

      console.log(error.response.data)

      if("email" in error.response.data)
        reasonError=error.response.data.email;
      else if("password1" in error.response.data)
        reasonError=error.response.data.password1[0];
      else if("username" in error.response.data)
        reasonError=error.response.data.username;    

      return Promise.reject(new Error(reasonError));
    });
  }

  //Enregistrement d'un utilisateur
  async passwordReset(user) {

    return axios.post(API_URL + 'rest/reset-password/', {     
      email: user.email, 
    }, {
        emulateJSON: true  
      })
    .then(response => {  
      return response.data;
    })
    .catch ( function (error){

      var reasonError='Erreur inconnue';

      //console.log(error.response.data)

      if("email" in error.response.data)
        reasonError=error.response.data.email;
      else if("password1" in error.response.data)
        reasonError=error.response.data.password1[0];
      else if("username" in error.response.data)
        reasonError=error.response.data.username;    

      return Promise.reject(new Error(reasonError));
    });
  }
}

export default new AuthService();