import Vue from "vue";
import Router from "vue-router";
import store from "../store";

Vue.use(Router);

//Les différentes routes et les rôles pouvant y accéder
const router = new Router({
  routes: [
    {
      path: "/login",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "login" */ "@/views/Login.vue"),
      meta: {
        allowAnonymous: true,
        user_group: ["", "admin"],
      },
    },
    {
      path: "/register",
      component: () =>
        import(/* webpackChunkName: "register" */ "@/views/Register.vue"),
      meta: {
        allowAnonymous: true,
        user_group: ["", "admin"],
      },
    },
    {
      path: "/scan",
      name: "Scan",
      component: () =>
        import(/* webpackChunkName: "scan" */ "@/views/Scan.vue"),
      meta: {
        disableScroll: true,
        allowAnonymous: true,
        user_group: ["", "admin"],
      },
    },
    {
      path: "/",
      name: "map",
      component: () =>
        import(/* webpackChunkName: "map" */ "@/views/MapView.vue"),
      meta: {
        allowAnonymous: true,
        user_group: ["", "admin"],
      },
    },
    {
      path: "/contribute",
      name: "contribute",
      component: () =>
        import(/* webpackChunkName: "contribute" */ "@/views/Contribute.vue"),
      meta: {
        allowAnonymous: false,
        user_group: ["", "admin"],
      },
    },
    {
      path: "/info",
      name: "info",
      component: () =>
        import(/* webpackChunkName: "info" */ "@/views/Info.vue"),
      meta: {
        allowAnonymous: true,
        user_group: ["", "admin"],
      },
    },
    {
      path: "/profile",
      name: "profile",
      component: () =>
        import(/* webpackChunkName: "profile" */ "@/views/Profile.vue"),
      meta: {
        allowAnonymous: false,
        user_group: ["", "admin"],
      },
    },
    {
      path: "/espace_pro",
      name: "espacepro",
      component: () =>
        import(/* webpackChunkName: "espacepro" */ "@/views/Profile.vue"),
      meta: {
        allowAnonymous: false,
        user_group: ["admin"],
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  //recupère l'information sur un utilisateur
  let loggedIn = localStorage.getItem("user");
  var userRole = "";

  if (loggedIn != null) {
    //console.log(loggedIn)
    if (
      "roles" in JSON.parse(loggedIn).user &&
      JSON.parse(loggedIn).user.roles[0] != null
    )
      userRole = JSON.parse(loggedIn).user.roles[0];
    //console.log(userRole);

    //recupérer la date de login
    //si plus de 30 jours alors logout
    let dateUserloggin = new Date(JSON.parse(loggedIn).date);
    dateUserloggin.setDate(dateUserloggin.getDate() + 29);

    let actualDate = new Date();

    if (actualDate > dateUserloggin) {
      loggedIn = null;
      store.dispatch("auth/logout").then(() => {
        console.log("logoutuser");
      });
    }
  }

  //Si on arrive sur login ou que nous sommes pas loggués
  if (to.name == "login" && loggedIn != null) {
    next({ path: "/" });
  } else if (!to.meta.allowAnonymous && loggedIn == null) {
    next({
      path: "/login",
      query: { redirect: to.fullPath },
    });
  } else {
    if (to.meta.user_group.includes(userRole)) next();
    else window.alert("Vous n'avez pas la permission");
  }
});

export default router;
