from django.shortcuts import render
from django.utils.translation import gettext_lazy as _

VIEW_ERRORS = {
    404: {'title': _("404 - Page not found"),
          'content': _("A 404 Not found error indicates..."), },
    500: {'title': _("Internal error"),
          'content': _("A 500 Internal error means..."), },
    403: {'title': _("Permission denied"),
          'content': _("A 403 Forbidden error means ..."), },
    400: {'title': _("Bad request"),
          'content': _("A 400 Bad request error means ..."), }, }


def error_view_handler(request, exception, status):
    return render(request, template_name='index.html', status=status,
                  context={'error': VIEW_ERRORS[status]['title']})


def error_404_view_handler(request, exception=None):
    return error_view_handler(request, exception, 404)


def error_500_view_handler(request, exception=None):
    return error_view_handler(request, exception, 500)


def error_403_view_handler(request, exception=None):
    return error_view_handler(request, exception, 403)


def error_400_view_handler(request, exception=None):
    return error_view_handler(request, exception, 400)