from django.contrib import admin
from django.conf.urls import include, url


handler404 = 'OuJeTrie.views.error_404_view_handler'
handler500 = 'OuJeTrie.views.error_500_view_handler'
handler403 = 'OuJeTrie.views.error_403_view_handler'
handler400 = 'OuJeTrie.views.error_400_view_handler'
# urls
urlpatterns = [
    url(r'^', include('OuJeTrie_API.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^maintenance-mode/', include('maintenance_mode.urls')),
]