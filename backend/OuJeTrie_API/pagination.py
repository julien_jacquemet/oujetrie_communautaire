from rest_framework.pagination import PageNumberPagination
from django.conf import settings

class CustomPagination(PageNumberPagination):
    page_size = settings.PAGINATION_MAX_PAGE_SIZE
    page_size_query_param = 'page_size'
