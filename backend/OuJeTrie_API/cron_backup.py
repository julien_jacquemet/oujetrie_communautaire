import os
from django.core import management
from django.conf import settings
from django_cron import CronJobBase, Schedule


class OuJeTrieCronJobBackup(CronJobBase):
    RUN_EVERY_MINS = 60*24 # toutes les heures
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'OuJeTrie_api.OuJeTrie_cron_job_backup'

    def do(self):
        management.call_command('dbbackup')
        management.call_command('mediabackup')