from django.apps import AppConfig


class OuJeTrieApiConfig(AppConfig):
    name = 'OuJeTrie_API'
