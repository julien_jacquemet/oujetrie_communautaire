from django.shortcuts import render


def index(request):
    return render(request, 'index.html')


def index_error(request):
    return render(request, 'index.html', {"error": "Merci d'utiliser l'application pour cette fonctionnalitée"})
