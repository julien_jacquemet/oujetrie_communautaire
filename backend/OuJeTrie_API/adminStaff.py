from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.admin import AdminSite

from django.conf import settings
from django.db.models import Q

import os

from . import models
from . import admin as admin_superstaff

# Register your models here.


def get_readonly_fields_with_append(self, request, obj=None):
    if obj and hasattr(self, 'append_readonly_fields'):  # editing an existing object
        return self.readonly_fields + self.append_readonly_fields
    return self.readonly_fields

class CommunauteCommuneAdmin(admin_superstaff.CommunauteCommuneAdmin):
    append_readonly_fields = ()
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(pk__in=cc)
        return None

class CommuneAdmin(admin_superstaff.CommuneAdmin):
    append_readonly_fields = ("communauteCommune",)
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(communauteCommune__in=cc)
        return None


class ConteneurAdmin(admin_superstaff.ConteneurAdmin):
    append_readonly_fields = ()
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(pav__commune__communauteCommune__in=cc)
        return None


class InciviliteAdmin(admin_superstaff.InciviliteAdmin):
    append_readonly_fields = ()
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(pav__commune__communauteCommune__in=cc)
        return None


class UploadedFileAdmin(admin_superstaff.UploadedFileAdmin):
    append_readonly_fields = ("user", "date",)
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(Q(incivilite__pav__commune__communauteCommune__in=cc) | Q(pav__commune__communauteCommune__in=cc))
        return None


class FillingLevelAdmin(admin_superstaff.FillingLevelAdmin):
    append_readonly_fields = ()
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(conteneur__pav__commune__communauteCommune__in=cc)
        return None


class PavAdmin(admin_superstaff.PavAdmin):
    append_readonly_fields = ("commune",)
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(commune__communauteCommune__in=cc)
        return None


class SignalementAdmin(admin_superstaff.SignalementAdmin):
    append_readonly_fields = ("pav",)
    get_readonly_fields = get_readonly_fields_with_append

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        if request.user.has_perm('OuJeTrie_API.staff_commune'):
            cc = request.user.userstaff.communauteCommune.all()
            return qs.filter(pav__commune__communauteCommune__in=cc)
        return None


class EntrepriseAdmin(admin_superstaff.EntrepriseAdmin):
    append_readonly_fields = ()
    get_readonly_fields = get_readonly_fields_with_append


class ConfigAppAdmin(admin_superstaff.ConfigAppAdmin):
    append_readonly_fields = ("communauteCommune",)
    get_readonly_fields = get_readonly_fields_with_append


class StaffAdminSite(AdminSite):
    site_header = "Page d'administration des PAV"
    site_title = "Admin PAV"
    index_title = "Admin PAV"


staff_admin_site = StaffAdminSite(name='event_admin')

staff_admin_site.register(models.CommunauteCommune, CommunauteCommuneAdmin)
staff_admin_site.register(models.Conteneur, ConteneurAdmin)
staff_admin_site.register(models.Commune, CommuneAdmin)
staff_admin_site.register(models.Pav, PavAdmin)
#staff_admin_site.register(models.CommunauteCommune, CommunauteCommuneAdmin)
staff_admin_site.register(models.UploadedFile, UploadedFileAdmin)
staff_admin_site.register(models.Incivilite, InciviliteAdmin)
staff_admin_site.register(models.FillingLevel, FillingLevelAdmin)
staff_admin_site.register(models.Signalement, SignalementAdmin)
#staff_admin_site.register(models.Entreprise, EntrepriseAdmin)
#staff_admin_site.register(models.ConfigMenuAppCitoyen, ConfigAppAdmin)


# Passage sur le systeme de login de AllAuth pour qu'il utilise ACCOUNT_LOGIN_ATTEMPTS_LIMIT et ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT
# On bloquer un possible bruteforce
staff_admin_site.login = login_required(staff_admin_site.login)


