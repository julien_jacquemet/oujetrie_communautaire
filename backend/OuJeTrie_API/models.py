from django.db import models
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.contrib.auth.models import User
from datetime import datetime

from .fields.DayOfWeek import DayOfTheWeekField, DAY_OF_THE_WEEK

import time
import pathlib
import os
import ntpath

from django.conf import settings
# Create your models here.


def get_file_path(instance, filename):
    if not os.path.isdir(settings.MEDIA_ROOT):
        os.mkdir(settings.MEDIA_ROOT)
    return '{0}/{1}_{2}{3}'.format(settings.MEDIA_ROOT, str(instance.user.id), str(int(time.time())), pathlib.Path(filename).suffix)


class UploadedFile(models.Model):
    file = models.FileField(upload_to=get_file_path)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def delete(self, using=None, keep_parents=False):
        if os.path.exists(self.file.path):
            os.remove(self.file.path)
        super(UploadedFile, self).delete()

    def __str__(self):
        return ntpath.basename(self.file.path) + " by " + self.user.username + " (" + self.date.strftime("%d/%m/%Y %H:%M:%S") + ")"


class CommunauteCommune(models.Model):
    name = models.TextField()  # non null
    region = models.TextField()  # non null
    departement = models.TextField()  # non null

    epci = models.TextField()
    # permet de savoir ce qu'on trie dans la poubelle jaune
    contentYelloWaste = models.TextField(blank=True, null=True)
    # permet de savoir ce qu'on trie dans la poubelle bleu
    contentBlueWaste = models.TextField(blank=True, null=True)
    # permet de savoir ce qu'on trie dans la poubelle verte
    contentGreenWaste = models.TextField(blank=True, null=True)
    # json a afficher dans un html pour savoir comment on trie
    desc = models.TextField(blank=True, null=True)
    # liste des activites en lien avec le tri
    listeActivity = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = 'CommunauteCommune'

    def __str__(self):
        return self.name


class Commune(models.Model):
    name = models.TextField()  # non null
    siren = models.TextField()  # non null
    postalCode = models.TextField()  # non null
    communauteCommune = models.ForeignKey(CommunauteCommune, on_delete=models.CASCADE) # FK >- CommunauteCommune.CCID #non null
    nbPaV = models.IntegerField                      # !!!!!!!!!!!!!!!
    # definition de la lcalisation de la mairie ?
    position = models.PointField("Location")
    # date de lev�e des ordures m�nag�res
    dateLeveePoubelle = models.TextField(blank=True, null=True)
    # heure de lev�e des ordure m�nag�re
    heureLeveePoubelle = models.TextField(blank=True, null=True)
    # Permet de savoir o� va le tri, certaines communes ne suivent peut-�tre pas
    # les recommandatations des ComCom
    contentYelloWaste = models.TextField(blank=True, null=True)  # vide si la commune est en coherence avec la communuate com
    contentBlueWaste = models.TextField(blank=True, null=True)  # vide si la commune est en coherence avec la communuate com
    contentGreenWaste = models.TextField(blank=True, null=True)  # vide si la commune est en coherence avec la communuate com
    # Liste des activites en lien avec le tri
    listeActivity = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = 'Commune'

    def __str__(self):
        return self.name + ' (' + str(self.postalCode) + ')'

class Entreprise(models.Model):
    name = models.TextField()  # non null
    region = models.TextField()  # non null
    departement = models.TextField()  # non null 
    # json a afficher dans un html pour savoir comment on trie
    desc = models.TextField(blank=True, null=True)
    # liste des activites en lien avec le tri
    listeActivity = models.TextField(blank=True, null=True)
    d1_name = models.CharField(blank=False, max_length=128)
    d1_type = models.TextField(blank=False) 
    d1_type_emplacement = models.TextField(blank=False) 
    d1_contenu = models.TextField(blank=False) 
    d1_contenant = models.TextField(blank=False) 
    d1_apparence = models.TextField(blank=False) 
    d1_description_gen = models.TextField(blank=True, null=True) 
    d1_description_tri = models.TextField(blank=True, null=True) 

    class Meta:
        verbose_name = 'Entreprise'

    def __str__(self):
        return self.name

class Incivilite(models.Model):
    users = models.ManyToManyField(User, blank=True)
    photo = models.ForeignKey(UploadedFile, on_delete=models.SET_NULL, blank=True, null=True)
    poubelleSauvage = models.BooleanField(default=False)
    pavAbime = models.BooleanField(default=False)
    encombrant = models.BooleanField(default=False)
    debordement = models.BooleanField(default=False)
    comment = models.TextField(blank=True)
    votingGood = models.IntegerField( default=0, blank=True)
    votingBad = models.IntegerField( default=0, blank=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    etatTraitement = models.TextField(blank=True)
    position = models.PointField("Location", null=True, default=Point(0,0))

    def delete(self):
        if self.photo:
            if (len(self.photo.incivilite_set.all()) + len(self.photo.pav_set.all())) <= 1:
                self.photo.delete()
        super(Incivilite, self).delete()

    class Meta:
        verbose_name = 'Incivilite'

    def __str__(self):
        first_elem = self.users.first()
        if first_elem:
            return first_elem.username + " (" + self.date.strftime("%d/%m/%Y %H:%M:%S") + ")"
        return "Non defini"


class FillingLevel(models.Model):
    users = models.ManyToManyField("auth.User", through='UserVoteLevel', related_name='levelby', blank=True)
    level = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def delete(self):
        for vote in self.uservotelevel_set.all():
            vote.delete()

        super(FillingLevel, self).delete()

class UserVoteLevel(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    fillingLevel = models.ForeignKey(FillingLevel, on_delete=models.DO_NOTHING)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    level = models.IntegerField(default=0)


class Conteneur(models.Model):
    type = models.TextField()  # enterr�, ...
    type_emplacement = models.TextField(default="", blank=True, null=True)
    content = models.TextField()  # contient le contenu de ContentYelloWaste ou les autres
    color = models.TextField(default="", blank=True, null=True)
    apparence = models.TextField(default="", blank=True, null=True)
    state = models.TextField(blank=True, null=True)  # peut contenir les valeurs suivantes ..
    # contient le niveau de remplissage du conteneur, cette valeur doit se
    # calculer par rapport � la moyenne des informations de la table FillingLevel
    # pour ce ContID.
    # Ou a supprimer et faire le calcul depuis la table FillingLevel
    fillingLevel = models.ForeignKey(FillingLevel, on_delete=models.CASCADE, blank=True, null=True)
    valide = models.BooleanField()
    dateLeveePassageCont = models.TextField(blank=True, null=True)
    heureLeveePassageCont = models.TextField(blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    appartenance_ext = models.ForeignKey(Entreprise, on_delete=models.DO_NOTHING, blank=True, null=True)
    accesPMR = models.BooleanField(default=False)
    volume = models.IntegerField( default=0, blank=True)
    frequenceCollecte = models.CharField(blank=True, max_length=10, null= True)

    def save(self, *args, **kwargs):
        # Si FillingLevel n'est pas defini, on cree l'object
        if self.fillingLevel is None:
            fl = FillingLevel()
            fl.save()
            self.fillingLevel = fl
            self.save()

        super(Conteneur, self).save(*args, **kwargs)

    def delete(self):
        self.fillingLevel.delete()
        for levee in self.levee_set.all():
            levee.delete()
        if self.fillingLevel:
            for vote in self.fillingLevel.uservotelevel_set.all():
                vote.delete()
        super(Conteneur, self).delete()

    class Meta:
        verbose_name = 'Conteneur'

    def __str__(self):
        pav = "INCONNU"
        commune = "INCONNU"
        comcom = "INCONNU"
        list_pav = self.pav_set.all()
        if len(list_pav) > 0:
            pav = list_pav[0].name
            if list_pav[0].commune:
                commune = list_pav[0].commune.name
                if list_pav[0].commune.communauteCommune:
                    comcom = list_pav[0].commune.communauteCommune.name

        return self.type + "_" + pav + "_" + commune + "_" + comcom


class Levee(models.Model):
    conteneur = models.ForeignKey(Conteneur, on_delete=models.CASCADE)
    day = DayOfTheWeekField()
    hour = models.TimeField()
    autoReset = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        super(Levee, self).save(*args, **kwargs)
        levees = self.conteneur.levee_set.all()
        if len(levees) > 0:
            levees = levees.order_by('day')
            str_levees_jours = ""
            str_levees_heures = ""
            timeformat = '%H:%M:%S'
            if settings.TIME_INPUT_FORMATS:
                timeformat = settings.TIME_INPUT_FORMATS[0]
            for levee in levees:
                if len(str_levees_jours) > 0 or len(str_levees_heures) > 0:
                    str_levees_jours += settings.SEPARATEUR_CONTENEUR
                    str_levees_heures += settings.SEPARATEUR_CONTENEUR
                str_levees_jours += DAY_OF_THE_WEEK[levee.day].capitalize()
                str_levees_heures += levee.hour.strftime(timeformat)
            self.conteneur.dateLeveePassageCont = str_levees_jours
            self.conteneur.heureLeveePassageCont = str_levees_heures
            self.conteneur.save()


class Pav(models.Model):
    name = models.TextField()
    position = models.PointField("Location")  # sous le format POINT(45 5) donc non null
    address = models.TextField()  # non null
    # commune = models.ManyToManyField(Commune, blank=True)
    commune = models.ForeignKey(Commune, on_delete=models.DO_NOTHING, blank=True, null=True)
    conteneurs = models.ManyToManyField(Conteneur, blank=True)
    incivilite = models.ManyToManyField(Incivilite, blank=True)
    valide = models.BooleanField(default=False)  # false par defaut si cr�er par user, vrai quand valid� par ??
    state = models.TextField(blank=True, null=True)  # l'etat du PAV, inexistant, endommag�
    dateLeveePav = models.TextField(blank=True, null=True)
    heureLeveePav = models.TextField(blank=True, null=True)
    photo = models.ForeignKey(UploadedFile, on_delete=models.SET_NULL, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    appartenance_ext = models.ForeignKey(Entreprise, on_delete=models.DO_NOTHING, blank=True, null=True)

    def delete(self):
        for conteneur in self.conteneurs.all():
            conteneur.delete()
        for incivilite in self.incivilite.all():
            incivilite.delete()
        for signalement in self.signalement_set.all():
            signalement.delete()

        if self.photo:
            if (len(self.photo.incivilite_set.all()) + len(self.photo.pav_set.all())) <= 1:
                self.photo.delete()
        super(Pav, self).delete()

    class Meta:
        verbose_name = 'PAV'

    def __str__(self):
        return self.name


class UserPassage(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    pav = models.ForeignKey(Pav, on_delete=models.CASCADE)
    photo = models.ForeignKey(UploadedFile, on_delete=models.SET_NULL, blank=True, null=True)
    comment = models.TextField(blank=True)
    position = models.TextField()
    date = models.DateTimeField(auto_now_add=True, blank=True)
    photoTicket = models.BinaryField()

    class Meta:
        verbose_name = 'UserPassage'


class ProductCode(models.Model):
    productCode = models.TextField()
    productName = models.TextField()
    packaging = models.TextField()
    date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        verbose_name = 'ProductCode'


class Signalement(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    type = models.TextField()

    pav = models.ForeignKey(Pav, on_delete=models.DO_NOTHING)

    commentaire = models.TextField()

    etatTraitement = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Signalement'

    def __str__(self):
        return self.type + " by " + self.user.username + " (" + self.date.strftime("%d/%m/%Y %H:%M:%S") + ")"


class Dechet(models.Model):
    name = models.CharField(blank=False, max_length=128)
    type = models.TextField(blank=False) 
    type_emplacement = models.TextField(blank=False) 
    contenu = models.TextField(blank=False) 
    contenant = models.TextField(blank=False) 
    apparence = models.TextField(blank=False) 
    description_gen = models.TextField(blank=True) 
    description_tri = models.TextField(blank=True) 
    motClesDechet = models.TextField(blank=True) 
    photo_tri = models.BinaryField()
    communauteCommune = models.ForeignKey(CommunauteCommune, on_delete=models.DO_NOTHING, blank=True, null=True, related_name="dechet")
    entreprise = models.ForeignKey(Entreprise, on_delete=models.DO_NOTHING, blank=True, null=True)

    class Meta:
        verbose_name = 'Dechet'


class ConfigMenuAppCitoyen(models.Model):
    nomMenu = models.CharField(blank=False, max_length=50)
    affichage = models.BooleanField(default=True)
    communauteCommune = models.ForeignKey(CommunauteCommune, on_delete=models.DO_NOTHING, blank=True, null=True, related_name="config")

    class Meta:
        verbose_name = 'ConfigMenuAppCitoyen'

class DemandeAjoutCom(models.Model):
    commune = models.ForeignKey(Commune, on_delete=models.DO_NOTHING, blank=True, null=True, related_name="AjoutCom")
    communauteCommune = models.ForeignKey(CommunauteCommune, on_delete=models.DO_NOTHING, blank=True, null=True, related_name="AjoutComCom")
    position = models.PointField("Location", null=True, default=Point(0,0))
    description = models.TextField(blank=True, null=True)     

    class Meta:
        verbose_name = 'DemandeAjoutCom'
