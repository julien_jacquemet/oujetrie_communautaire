
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
        $.fn.multiline = function(text){
        this.text(text);
        this.html(this.html().replace(/\n/g,'<br/>'));
        return this;
    }

    $('#new_password2').keyup(function(){
        if ($("#new_password1").val() !== $(this).val()) {
            $(this).parent().attr("data-validate","Mot de passe different");
            showValidate(this);
        }
        else {
            $(this).parent().attr("data-validate","Mot de passe requis");
            hideValidate(this);
        }
    });

    $("#idForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        let form = $(this);
        let password1=$("#new_password1").val();
        let password2=$("#new_password2").val();
        if (password1 !== password2)
        {
            $("#resultError").text("Les mots de passe ne sont pas identiques");
        }
        else if (form.length>0)
        {
            $.ajax({
                type: "POST",
                url: form[0].action,
                data: form.serialize(), // serializes the form's elements.
                success: function (xhr, status, error) {
                    //alert("success"); // show response from the php script.
                    $("#resultSucces").text("Changement du mot de passe fait avec succès. Redirection dans 3s.");
                    var timer = setTimeout(function() {
                        window.location='https://OuJeTrie.fr'
                    }, 3000);
                },
                error: function (xhr, status, error) {
                    //alert("error"); // show response from the php script.
                    const obj = JSON.parse(xhr.responseText);
                    let error_str = "";

                    if ("token" in obj)
                    {
                        if (error_str.length>0) error_str += "\n";
                        error_str += "Le token est expiré ou invalide.";
                    }

                    if ("uid" in obj)
                    {
                        if (error_str.length>0) error_str += "\n";
                        error_str += "\n"+"L'uid est expiré ou invalide.";
                    }

                    if ("new_password1" in obj)
                    {

                        obj["new_password1"].forEach(function (item, index) {
                            if (error_str.length>0) error_str += "\n";
                            error_str += item;
                        });
                    }
                    if ("new_password2" in obj )
                    {
                        let error = "";
                        obj["new_password2"].forEach(function (item, index) {
                            if (error_str.length>0) error_str += "\n";
                            error_str += item;
                        });
                    }

                    $("#resultError").multiline(error_str);
                }
            });
        }
    });

})(jQuery);