from django.urls import path, re_path
from . import views

from . import adminStaff

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url

from .login import views as login_view
from knox import views as knox_views
from allauth.account.views import confirm_email, email_verification_sent, login

from .index import index, index_error
from dj_rest_auth.views import PasswordResetView, PasswordResetConfirmView

urlpatterns = [
    # Index
    path('', index, name='index'),


    # GET
    re_path(r'^api/v1/pavs/(?P<pk>[0-9]+)$',  # Url to get update or delete a pav
            views.GetUniquePav.as_view(),
            name='GetUnique_pav'),

    re_path(r'^api/v1/pavs/(?P<type>[a-z]+)/(?P<pk>[0-9]+)$',  # Url to get update or delete a pav
            views.GetPavsByCom_ComCom.as_view(),
            name='Get_pavs_by_com_comcom'),

    re_path(r'^api/v1/product/(?P<pk>[0-9]+)$',  # Url to get update or delete a pav
            views.GetProductInfo.as_view(),
            name='GetProductInfo'),

    re_path(r'^api/v1/incivilites/(?P<pkPav>[0-9]+)$',  # Url to get update or delete a pav
            views.GetIncivilites.as_view(),
            name='GetIncivilites'),

    re_path(r'^api/v1/uservote/conteneur/(?P<pk>[0-9]+)$',  # Url to get update or delete a pav
          views.GetUserVoteByConteneur.as_view(),
          name='GetUserVote_conteneur'),

    path('api/v1/pavs/',
         views.GetPav.as_view(),
         name='GetPav'),

    path('api/v1/pavsn/',
         views.GetPavNearest.as_view(),
         name='GetPav_nearest'),

    path('api/v1/commune/',
         views.GetCommune.as_view(),
         name='GetCommune'),

    path('api/v1/communautecom/',
         views.GetCommunauteCom.as_view(),
         name='GetCommunauteCom'),

    path('api/v1/commune/search',  # Url to get update or delete a pav
         views.GetCommuneSearch.as_view(),
         name='GetCommune_search'),

    path('api/v1/communautecom/search',  # Url to get update or delete a pav
         views.GetCommunauteCom.as_view(),
         name='GetCommunauteCom'),

    path('api/v1/conts/',
         views.GetConteneur.as_view(),
         name='GetConteneur'),

    re_path(r'^api/v1/conts/(?P<pk>[0-9]+)$',
        views.GetConteneur.as_view(),
        name='GetConteneurById'),


    path('api/v1/configappcitoyen/',
         views.GetConfigAppCitoyen.as_view(),
         name='GetConfigApp'),


    path('api/v1/entreprise/',
         views.GetEntreprise.as_view(),
         name='GetEntreprise'),     
     
     path('api/v1/demandeajoutcom/',
         views.GetDemandeAjoutComSearch.as_view(),
         name='GetDemandeAjoutCom'),



    # POST
    path('api/v1/set/pavs/',
         views.PostPav.as_view(),
         name='PostPav'),
    path('api/v1/set/cont/',
         views.PostConteneur.as_view(),
         name='PostConteneur'),

    path('api/v1/upload/',
         views.PostUpload.as_view(),
         name='PostUpload'),

    path('api/v1/add/inci/',
         views.PostIncivilite.as_view(),
         name='PostIncivilite'),

    path('api/v1/vote/inci/',
         views.PostVoteIncivilite.as_view(),
         name='PostVote_incivilite'),

    path('api/v1/update/fillinglevel/',
         views.PostUpdateFillingLevel.as_view(),
         name='PostUpdate_FillingLevel'),

    path('api/v1/add/anomalie/',
         views.PostSignalement.as_view(),
         name='PostSignalement'),

    re_path(r'^api/v1/anomalies/(?P<type>[a-z]+)/(?P<pk>[0-9]+)$',  # Url to get update or delete a pav
            views.GetSignalementsByCom_ComCom.as_view(),
            name='Get_signalements_by_com_comcom'),


     path('api/v1/add/demandeajoutcom/',
         views.PostDemandeAjoutCom.as_view(),
         name='PostDemandeAjoutCom'),

    # Auth
    url(r'^auth/list/',
        login_view.ListSessionView.as_view()),

    url(r'^auth/registration/',
        login_view.RegisterView.as_view()),
    url(r'^auth/login/',
        login_view.LoginView.as_view(), name='knox_login'),
    url(r'^auth/logout/',
        knox_views.LogoutView.as_view(), name='knox_logout'),

    url(r'^auth/logoutall/',
        knox_views.LogoutAllView.as_view(), name='knox_logoutall'),


    # Confirme e-mail
    path("auth/confirm-email/",
         email_verification_sent,
         name="account_email_verification_sent"),

    re_path(r"^auth/confirm-email/(?P<key>[-:\w]+)/$",
            confirm_email,
            name="account_confirm_email"),

    # Lost password
    path('auth/rest/reset-password/',
         PasswordResetView.as_view(),
         name='rest_password_reset'),

    path('auth/rest/reset-password/confirm/',
         PasswordResetConfirmView.as_view(),
         name='rest_password_reset_confirm'),

    path('auth/reset-password/confirm/<uidb64>/<token>/',
         login_view.getResetPasswordPage,
         name="password_reset_confirm"),

    path("accounts/login/", login, name="account_login"),


    # Pages bloquées
    path("accounts/signup/", index_error, name="account_signup"),
    path("password/change/", index_error, name="account_reset_password"),

    url('staff/', adminStaff.staff_admin_site.urls),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
