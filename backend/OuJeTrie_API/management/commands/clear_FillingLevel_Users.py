from django.core.management import BaseCommand
from django.utils import timezone
from django.db.models import Q
from django.conf import settings

from OuJeTrie_API.models import FillingLevel, Conteneur, Levee
from datetime import datetime, timedelta


class Command(BaseCommand):
    help = "Supprimer les Users qui ont participé au FillingLevel"
    nbMinuteSecur = 2  # nombre de minutes que l'on va prendre en plus pour etre sur de ne pas loupé un vote qui serait trop proche du dernier passage

    def add_arguments(self, parser):
        parser.add_argument('type', type=str, help='Type de nettoyage: all/expired/{IdConteneur}')

    def clear_fl(self, fillingl):
        if len(fillingl.uservotelevel_set.all()):
            for vote in fillingl.uservotelevel_set.all():
                vote.delete()
            fillingl.level = 0
            fillingl.date = datetime.now()
            fillingl.save()

    def handle(self, *args, **kwargs):
        type_str = kwargs['type']

        if type_str == "all":
            for fillingL in FillingLevel.objects.all():
                self.clear_fl(fillingL)
        elif type_str == "expired":
            endtime_dt = timezone.localtime(timezone.now())
            endtime = endtime_dt.time()
            starttime_dt = endtime_dt - timedelta(minutes=settings.FILLINGLEVEL_INTERVAL_CLEAR) - timedelta(minutes=self.nbMinuteSecur)
            starttime = starttime_dt.time()

            current_day = datetime.today().isoweekday()
            if starttime <= endtime:
                levees = Levee.objects.filter(Q(day=current_day), Q(hour__range=(starttime, endtime)), Q(autoReset=True))
            else:
                prev_day = datetime.today().isoweekday()-1
                if prev_day <= 0:
                    prev_day = 7
                levees = Levee.objects.filter(Q(day=prev_day, hour__range=(starttime, datetime.max.time()), autoReset=True) |
                                              Q(day=current_day, hour__range=(datetime.min.time(), endtime), autoReset=True))
            if levees.count() > 0:
                for levee in levees:
                    self.clear_fl(levee.conteneur.fillingLevel)
        else:
            try:
                conteneur = Conteneur.objects.get(pk=int(type_str))
                fillingl = conteneur.fillingLevel
            except Conteneur.DoesNotExist:
                self.stdout.write("Error in Conteneur ID (not exist)")
                return
            except:
                self.stdout.write("Error in Conteneur ID format")
                return
            self.clear_fl(fillingl)
