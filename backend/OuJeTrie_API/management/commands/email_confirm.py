from django.core.management.base import BaseCommand
from allauth.account.models import EmailAddress
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Permet de confirmer un email manuellement'

    def add_arguments(self, parser):
        parser.add_argument('email', type=str, help='Email a confirmer')

    def handle(self, *args, **kwargs):
        email_str = kwargs['email']
        email_not_existe = False
        try:
            email_obj = EmailAddress.objects.get(email__iexact=email_str)
            if email_obj.verified:
                self.stdout.write("Email déjà confirmé")
                return
            email_obj.verified = True
            email_obj.save()
        except EmailAddress.DoesNotExist:
            email_not_existe = True

        if email_not_existe:
            try:
                user_obj = User.objects.get(email__iexact=email_str)
                if user_obj:
                    # On ajoute l'email dans la liste des emails
                    email_obj = EmailAddress()
                    email_obj.email = email_str
                    email_obj.verified = True
                    email_obj.primary = True
                    email_obj.user_id = user_obj.id
                    email_obj.save()
                else:
                    self.stdout.write("User est null")
            except User.DoesNotExist:
                self.stdout.write("Cette email est utilisé par aucun User")
        self.stdout.write("Email confirmé avec succès")




