from knox.models import AuthToken
from OuJeTrie_API.custom.customtoken import AuthTokenAdv


def create_knox_token(token_model, user, serializer, platform=""):
    token = AuthToken.objects.create(user=user)
    advtoken = AuthTokenAdv.objects.create(token=token[0], platform=platform)
    return token
