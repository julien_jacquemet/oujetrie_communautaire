from rest_framework.response import Response

from dj_rest_auth.views import LoginView
from dj_rest_auth.registration.views import RegisterView

from allauth.account.utils import complete_signup
from allauth.account import app_settings as allauth_settings

from .serializers import KnoxSerializer
from .utils import create_knox_token

from dj_rest_auth.utils import jwt_encode
from django.conf import settings

from rest_framework.generics import ListAPIView

from rest_framework.renderers import JSONRenderer
from knox.auth import TokenAuthentication
from OuJeTrie_API.permissions import IsAuthenticated
from OuJeTrie_API.custom.customtoken import AuthTokenAdv
from OuJeTrie_API.custom.customtoken import TokenAdvSerializer
from rest_framework import status
from knox.models import AuthToken

from django.utils import timezone

from knox.settings import knox_settings


class LoginView(LoginView):
    def login(self):
        self.user = self.serializer.validated_data['user']
        token_limit_per_user = knox_settings.TOKEN_LIMIT_PER_USER
        if token_limit_per_user is not None:
            now = timezone.now()
            token = AuthToken.objects.all().filter(user=self.user).filter(expiry__gt=now)
            if token.count() >= token_limit_per_user:
                self.token_limit_reach = True
                return

        if getattr(settings, 'REST_USE_JWT', False):
            self.token = jwt_encode(self.user)
        else:
            platform_val = ""
            if "platform" in self.request.data:
                platform_val = self.request.data["platform"]

            self.token = create_knox_token(self.token_model, self.user,
                                      self.serializer, platform=platform_val)

        if getattr(settings, 'REST_SESSION_LOGIN', True):
            self.process_login()

    def get_response(self):
        serializer_class = self.get_response_serializer()

        if hasattr(self, 'token_limit_reach') and self.token_limit_reach:
            return Response(
                {"error": "Maximum amount of tokens allowed per user exceeded."},
                status=status.HTTP_403_FORBIDDEN)

        data = {
            'user': self.user,
            'token': self.token
        }
        serializer = serializer_class(instance=data, context={'request': self.request})

        return Response(serializer.data, status=200)


class RegisterView(RegisterView):
    def get_response_data(self, user):
        return KnoxSerializer({'user': user, 'token': self.token}).data

    def perform_create(self, serializer):
        user = serializer.save(self.request)
        platform_val = ""
        if "platform" in self.request.data:
            platform_val = self.request.data["platform"]
        self.token = create_knox_token(None, user, None, platform=platform_val)
        complete_signup(self.request._request, user, allauth_settings.EMAIL_VERIFICATION, None)
        return user



class ListSessionView(ListAPIView):
    renderer_classes = [JSONRenderer]
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        authtokens = AuthTokenAdv.objects.all()
        return authtokens

    def get_queryset_by_user(self,user):
        authtokens = AuthTokenAdv.objects.all().filter(token__user=user)

        return authtokens


    # Get all Pav
    def get(self, request, *args, **kwargs):
        authtokens = self.get_queryset_by_user(request.user)
        serializer = TokenAdvSerializer(authtokens, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


from django.shortcuts import render
def getResetPasswordPage(request, uidb64="", token=""):
    return render(request, 'password_reset.html', {"uidb64": uidb64, "token": token})
