from rest_framework import serializers
from OuJeTrie_API.serializers import UserSerializer as UserDetailsSerializer
from django.conf import settings
from dj_rest_auth.serializers import PasswordResetSerializer


class KnoxSerializer(serializers.Serializer):
    """
    Serializer for Knox authentication.
    """
    token = serializers.SerializerMethodField()
    user = UserDetailsSerializer()

    def get_token(self, obj):
        return obj["token"][1]


class CustomPasswordResetSerializer(PasswordResetSerializer):
    def save(self):
        request = self.context.get('request')
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
            'html_email_template_name': 'registration/password_reset_email.html'
        }

        opts.update(self.get_email_options())
        self.reset_form.save(**opts)