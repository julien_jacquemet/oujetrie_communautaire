from django.contrib import admin
from django.contrib.auth.decorators import login_required
from . import models

from django.contrib.gis.db import models as django_models
from .adminwidget.VerboseManyToManyRawId import VerboseManyToManyRawIdWidget
from django.conf import settings
import os

from import_export.admin import ImportExportActionModelAdmin
from import_export import resources

from django.utils.safestring import mark_safe
from django.db.models import Q

from mapwidgets.widgets import GooglePointFieldWidget
from . import serializers
import json
from django.http import HttpResponse

from import_export.admin import ExportActionMixin

from django.forms.models import model_to_dict
# Register your models here.


class LeveeInline(admin.StackedInline):
    model = models.Levee
    extra = 1

class DechetInline(admin.StackedInline):
    model = models.Dechet
    extra = 1


class CommunauteCommuneResource(resources.ModelResource):
    class Meta:
        model = models.CommunauteCommune
        exclude = ()
        skip_unchanged = True
        #fields = ('id', 'name',)

class EntrepriseResource(resources.ModelResource):
    class Meta:
        model = models.Entreprise
        exclude = ()
        skip_unchanged = True
        #fields = ('id', 'name',)


class ConfigAppPavResource(resources.ModelResource):
    class Meta:
        model = models.ConfigMenuAppCitoyen
        exclude = ()
        skip_unchanged = True
        #fields = ('id', 'name',)


def clear_commune(modeladmin, request, queryset):
    if queryset:
        for commune in queryset:
            for pav in commune.pav_set.all():
                pav.delete()


clear_commune.short_description = 'Supprimer tous les PAVs des communes selectionnées'


def clear_comcom(modeladmin, request, queryset):
    if queryset:
        for comcom in queryset:
            for commune in comcom.commune_set.all():
                for pav in commune.pav_set.all():
                    pav.delete()


clear_comcom.short_description = 'Supprimer tous les PAVs des comcoms selectionnées'


class CommunauteCommuneAdmin(ImportExportActionModelAdmin):
    search_fields = ('name', 'region', 'departement')
    list_display = ('name', 'region', 'departement')
    inlines = (DechetInline,)
    resource_class = CommunauteCommuneResource

    actions = (clear_comcom, )

class CommuneResource(resources.ModelResource):
    class Meta:
        model = models.Commune
        exclude = ()
        skip_unchanged = True


class CommuneAdmin(ImportExportActionModelAdmin):
    search_fields = ('name', 'postalCode', 'communauteCommune__name')
    list_display = ('name', 'postalCode', 'communauteCommune')
    raw_id_fields = ('communauteCommune',)

    actions = (clear_commune, )

    resource_class = CommuneResource

    formfield_overrides = {
        django_models.PointField: {"widget": GooglePointFieldWidget}
    }



class ConteneurResource(resources.ModelResource):
    class Meta:
        model = models.Conteneur
        exclude = ()
        skip_unchanged = True


class ConteneurAdmin(ImportExportActionModelAdmin):
    search_fields = ("pav__name", "pav__commune__name", "type")
    raw_id_fields = ('user', 'fillingLevel',)
    list_filter = ('date',)
    inlines = (LeveeInline,)

    resource_class = ConteneurResource

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            obj.delete()



class InciviliteAdmin(ImportExportActionModelAdmin):
    search_fields = ('pav__name', 'pav__commune__name')
    list_display = ('get_user', 'date', 'show_file_thumbnail', 'get_pav', 'get_commune')
    raw_id_fields = ('users', 'photo')
    list_filter = ('date',)
    readonly_fields = ("show_file",)

    formfield_overrides = {
        django_models.PointField: {"widget": GooglePointFieldWidget}
    }

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            obj.delete()


    def get_pav(self, obj):
        first_elem = obj.pav_set.first()
        if first_elem:
            return first_elem.name
        return 'Non defini'
    get_pav.short_description = 'Pav'
    get_pav.allow_tags = True

    def get_commune(self, obj):
        first_elem = obj.pav_set.first()
        if first_elem:
            return first_elem.commune.name
        return 'Non defini'
    get_commune.short_description = 'Commune'
    get_commune.allow_tags = True

    def show_file(self, obj):
        if obj.photo and obj.photo.file:
            return mark_safe(
                '<img src="{url}" width="512" />'.format(url=obj.photo.file.url)
            )
        else:
            return '(Pas de photo trouvé)'
    show_file.short_description = 'Affichage photo'
    show_file.allow_tags = True

    def show_file_thumbnail(self, obj):
        if obj.photo and obj.photo.file:
            if not settings.INCIVILITE_THUMBNAIL:
                return "Affichage désactivé"
            return mark_safe(
                '<img src="{url}" width="52" />'.format(url=obj.photo.file.url)
            )
        else:
            return '(Pas de photo trouvé)'
    show_file_thumbnail.short_description = 'Miniature'
    show_file_thumbnail.allow_tags = True

    def get_user(self, obj):
        return obj.users.first()
    get_user.short_description = 'User'

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        # Si le formfield est un ManyToMany, on utilise un widget custom pour l'affichage
        if db_field.many_to_many and db_field.name in self.raw_id_fields:
            kwargs['widget'] = VerboseManyToManyRawIdWidget(db_field.remote_field, self.admin_site)
        else:
            return super().formfield_for_dbfield(db_field, request, **kwargs)
        #kwargs.pop('request')
        return db_field.formfield(**kwargs)



class UploadedFileAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)
    readonly_fields = ("show_file",)

    def show_file(self, obj):
        if obj.file:
            return mark_safe(
                '<img src="{url}" width="512" />'.format(url=obj.file.url)
            )
        else:
            return '(Pas de photo trouvé)'
    show_file.short_description = 'Affichage photo'
    show_file.allow_tags = True


class FillingLevelAdmin(admin.ModelAdmin):
    search_fields = ("conteneur__type" "conteneur__pav__name", "conteneur__pav__commune__name")
    list_display = ('level', 'get_conteneur', 'get_pav')
    list_filter = ('date',)

    def get_conteneur(self, obj):
        first_elem = obj.conteneur_set.first()
        if first_elem:
            return first_elem
        return 'Non defini'
    get_conteneur.short_description = 'Conteneur'

    def get_pav(self, obj):
        first_elem = obj.conteneur_set.first()
        if first_elem:
            listPav = first_elem.pav_set.all()
            if len(listPav) > 0:
                return first_elem.pav_set.all().first().name
        return 'Non defini'
    get_pav.short_description = 'Pav'

class PavResource(resources.ModelResource):
    class Meta:
        model = models.Pav
        exclude = ()
        skip_unchanged = True


def export_pav(modeladmin, request, queryset):
    pav_list = []
    for pav in queryset:
        pav_dict = serializers.PavExportSerializer(pav).data
        pav_list.append(pav_dict)
    json_object = json.dumps(pav_list, ensure_ascii=False, indent=4)
    print(json_object)
    response = HttpResponse(json_object, content_type='application/json')
    response['Content-Disposition'] = 'attachment; filename="pav.json"'
    return response


export_pav.short_description = 'Exporter les PAV et leurs conteneurs'


class PavAdmin(ImportExportActionModelAdmin):
    search_fields = ('commune__postalCode', 'commune__name', 'commune__communauteCommune__name')
    list_filter = ('valide',)
    modifiable = True
    list_display = ('name', 'commune', 'comcom', 'valide')
    raw_id_fields = ('commune', 'photo', 'conteneurs', 'incivilite',)
    readonly_fields = ("show_file",)

    actions = (ExportActionMixin.export_admin_action, export_pav, )

    formfield_overrides = {
        django_models.PointField: {"widget": GooglePointFieldWidget}
    }

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            obj.delete()

    def show_file(self, obj):
        if obj.photo and obj.photo.file:
            return mark_safe(
                '<img src="{url}" width="512" />'.format(url=obj.photo.file.url)
            )
        else:
            return '(Pas de photo trouvé)'
    show_file.short_description = 'Affichage photo'
    show_file.allow_tags = True

    def comcom(self, obj):
        return obj.commune.communauteCommune.name

    resource_class = PavResource

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        # Si le formfield est un ManyToMany, on utilise un widget custom pour l'affichage
        if db_field.many_to_many and db_field.name in self.raw_id_fields:
            kwargs['widget'] = VerboseManyToManyRawIdWidget(db_field.remote_field, self.admin_site)
        else:
            return super().formfield_for_dbfield(db_field, request, **kwargs)

        return db_field.formfield(**kwargs)


class SignalementResource(resources.ModelResource):
    class Meta:
        model = models.Signalement
        exclude = ()
        skip_unchanged = True


class SignalementAdmin(ImportExportActionModelAdmin):
    search_fields = ('pav__commune__postalCode', 'pav__commune__name', 'pav__commune__communauteCommune__name')

    list_display = ('date', 'type', 'commentaire', 'pav')
    raw_id_fields = ('user', 'pav')
    list_filter = ('date',)

    resource_class = PavResource

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        # Si le formfield est un ManyToMany, on utilise un widget custom pour l'affichage
        if db_field.many_to_many and db_field.name in self.raw_id_fields:
            kwargs['widget'] = VerboseManyToManyRawIdWidget(db_field.remote_field, self.admin_site)
        else:
            return super().formfield_for_dbfield(db_field, request, **kwargs)
        #kwargs.pop('request')
        return db_field.formfield(**kwargs)


class EntrepriseAdmin(ImportExportActionModelAdmin):
    list_display = ('name', 'region', 'departement')
#    inlines = (DechetInline,)
    resource_class = EntrepriseResource


class ConfigAppAdmin(ImportExportActionModelAdmin):
    search_fields = ('nomMenu',)

    list_display = ('nomMenu', 'affichage', 'communauteCommune')
    raw_id_fields = ('communauteCommune',)

    resource_class = ConfigAppPavResource

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        # Si le formfield est un ManyToMany, on utilise un widget custom pour l'affichage
        if db_field.many_to_many and db_field.name in self.raw_id_fields:
            kwargs['widget'] = VerboseManyToManyRawIdWidget(db_field.remote_field, self.admin_site)
        else:
            return super().formfield_for_dbfield(db_field, request, **kwargs)
        #kwargs.pop('request')
        return db_field.formfield(**kwargs)


def has_permission(request):
    """
    Checks if the current user has access.
    """
    if request.user.is_authenticated and request.user.is_superuser:
        return True

    if not request.user.is_authenticated:
        return False

    return False


admin.site.register(models.Conteneur, ConteneurAdmin)
admin.site.register(models.Commune, CommuneAdmin)
admin.site.register(models.Pav, PavAdmin)
admin.site.register(models.CommunauteCommune, CommunauteCommuneAdmin)
admin.site.register(models.UploadedFile, UploadedFileAdmin)
admin.site.register(models.Incivilite, InciviliteAdmin)
admin.site.register(models.FillingLevel, FillingLevelAdmin)
admin.site.register(models.Signalement, SignalementAdmin)
admin.site.register(models.Entreprise, EntrepriseAdmin)
admin.site.register(models.ConfigMenuAppCitoyen, ConfigAppAdmin)
admin.site.register(models.DemandeAjoutCom)

# Affichage version
version = "Inconnu"
if os.path.isfile(settings.PATH_VERSION_FILE):
    f = open(settings.PATH_VERSION_FILE, "r")
    version = f.read()

admin.site.index_title = admin.site.index_title + " ( " + version + " )"

# Passage sur le systeme de login de AllAuth pour qu'il utilise ACCOUNT_LOGIN_ATTEMPTS_LIMIT et ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT
# On bloquer un possible bruteforce
admin.site.login = login_required(admin.site.login)
admin.site.has_permission = has_permission
