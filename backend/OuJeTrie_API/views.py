from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, ListAPIView
from .permissions import IsOwnerOrReadOnly, IsAuthenticated
from .pagination import CustomPagination

from . import serializers
from . import models

from .forms import UploadFileForm

import json
import datetime
import os

from django.contrib.gis.geos import GEOSGeometry, GEOSException
from django.contrib.gis.db.models.functions import Distance
from django.db.models import Q

from rest_framework.renderers import JSONRenderer

from django.conf import settings

from knox.auth import TokenAuthentication

from datetime import timedelta
from django.utils import timezone

import requests

#   ____ _____ _____
#  / ___| ____|_   _|
# | |  _|  _|   | |
# | |_| | |___  | |
#  \____|_____| |_|


class GetPav(ListAPIView):
    serializer_class = serializers.PavSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        pavs = models.Pav.objects.all()
        return pavs

    # Get all Pav
    def get(self, request, *args, **kwargs):
        pavs = self.get_queryset()
        paginate_queryset = self.paginate_queryset(pavs)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)


class GetCommunauteCom(ListAPIView):
    serializer_class = serializers.CommunauteCommuneSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        comcom = models.CommunauteCommune.objects.all()
        return comcom

    def search_by_name(self, search, limit):
        elem = self.get_queryset().filter(name__icontains=search)
        elem = elem[:limit]
        return elem

    def get(self, request, *args, **kwargs):
        limit = settings.DEFAULT_LIMIT_RESULT_SEARCH
        if "limit" in request.GET.keys():
            try:
                limit = int(request.GET["limit"])
            except:
                return Response({'error': 'Error value limit'}, status=status.HTTP_400_BAD_REQUEST)

        if "search" in request.GET.keys() and len(request.GET["search"]) < 2:
            return Response({'error': 'Error value search len < 2'}, status=status.HTTP_400_BAD_REQUEST)

        if "search" in request.GET.keys():
            elem = self.search_by_name(request.GET["search"], limit=limit)
            serializer = self.serializer_class(elem, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            # Utile ?
            comcom = self.get_queryset()
            paginate_queryset = self.paginate_queryset(comcom)
            serializer = self.serializer_class(paginate_queryset, many=True)
            return self.get_paginated_response(serializer.data)


class GetPavNearest(ListAPIView):
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    serializer_class = serializers.PavNearSerializer
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        pavs = models.Pav.objects.all()
        return pavs

    # Get all Pav
    def get(self, request, *args, **kwargs):
        if len(request.GET) > 0 and "position" in request.GET:
            limit = settings.DEFAULT_LIMIT_RESULT_SEARCH
            try:
                pnt = GEOSGeometry(request.GET["position"], srid=4326)
            except:
                return Response({'error': 'Error value position'}, status=status.HTTP_400_BAD_REQUEST)

            pav = self.get_queryset()
            pavnear = pav.annotate(
                distance=Distance('position', pnt)
            ).order_by('distance')

            if "radius" in request.GET:
                try:
                    radius = int(request.GET["radius"])
                except:
                    return Response({'error': 'Error value radius'}, status=status.HTTP_400_BAD_REQUEST)
                pavnear = pavnear.filter(distance__lte=radius)

            if "limit" in request.GET.keys():
                try:
                    limit = int(request.GET["limit"])
                except:
                    return Response({'error': 'Error value limit'}, status=status.HTTP_400_BAD_REQUEST)

            pavnear = pavnear[: limit]

            paginate_queryset = self.paginate_queryset(pavnear)
            serializer = self.serializer_class(paginate_queryset, many=True)
            return self.get_paginated_response(serializer.data)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class GetConteneur(ListAPIView):
    serializer_class = serializers.ConteneurSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]


    def get_queryset(self, pk=None):
        if pk:
            try:
                elem = models.Conteneur.objects.get(pk=pk)
            except models.Conteneur.DoesNotExist:
                return None
        else:
            elem = models.Conteneur.objects.all()
        return elem

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs:
            elem = self.get_queryset(kwargs["pk"])
            if not elem:
                content = {
                    'error': 'Not Found'
                }
                return Response(content, status=status.HTTP_404_NOT_FOUND)
            serializer = self.serializer_class(elem)
            return Response(serializer.data, status=status.HTTP_200_OK)

        conteneurs = self.get_queryset()
        conteneurs = conteneurs.order_by("pk")
        paginate_queryset = self.paginate_queryset(conteneurs)
        serializer = self.serializer_class(paginate_queryset, many=True, context=self.get_serializer_context())

        return self.get_paginated_response(serializer.data)

    def get_serializer_context(self):
        context = super(GetConteneur, self).get_serializer_context()
        context.update({"request": self.request})
        return context


class GetCommune(ListAPIView):
    serializer_class = serializers.CommuneSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        commu = models.Commune.objects.all()
        return commu

    # Get all Pav
    def get(self, request, *args, **kwargs):
        commu = self.get_queryset()
        paginate_queryset = self.paginate_queryset(commu)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)


class GetCommuneSearch(ListAPIView):
    serializer_class = serializers.CommuneSearchSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        commu = models.Commune.objects.all()
        return commu

    def search_by_name(self, search, pnt=None, postalcode=None, limit=20):
        elem = self.get_queryset().filter(name__icontains=search)

        if postalcode:
            elem = elem.filter(postalCode__icontains=postalcode)
        if pnt:
            elem = elem.annotate(
                distance=Distance('position', pnt)
            ).order_by('distance')
        return elem[:limit]

    def search_by_position(self, pnt, postalcode=None, limit=20):
        elem = self.get_queryset()
        if postalcode:
            elem = elem.filter(postalCode__icontains=postalcode)
        elem = elem.annotate(
            distance=Distance('position', pnt)
        ).order_by('distance')[:limit]

        return elem

    # GET a Commune
    def get(self, request, *args, **kwargs):
        limit = settings.DEFAULT_LIMIT_RESULT_SEARCH
        if "limit" in request.GET.keys():
            try:
                limit = int(request.GET["limit"])
            except:
                return Response({'error': 'Error value limit'}, status=status.HTTP_400_BAD_REQUEST)

        pnt = None

        if "search" in request.GET.keys() and len(request.GET["search"]) < 2:
            return Response({'error': 'Error value search len < 2'}, status=status.HTTP_400_BAD_REQUEST)
        elif "position" in request.GET.keys() and len(request.GET["position"]) == 0:
            return Response({'error': 'Error value position'}, status=status.HTTP_400_BAD_REQUEST)
        elif "postalcode" in request.GET.keys() and len(request.GET["postalcode"]) == 0:
            return Response({'error': 'Error value postalcode'}, status=status.HTTP_400_BAD_REQUEST)
        elif "position" in request.GET.keys():
            try:
                pnt = GEOSGeometry(request.GET["position"], srid=4326)
            except:
                return Response({'error': 'Error value position'}, status=status.HTTP_400_BAD_REQUEST)

        postalcode = None
        if "postalcode" in request.GET.keys():
            postalcode = request.GET["postalcode"]

        if "search" in request.GET.keys():
            elem = self.search_by_name(request.GET["search"], pnt, postalcode, limit=limit)
            serializer = self.serializer_class(elem, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        elif "position" in request.GET.keys():
            elem = self.search_by_position(pnt, limit=limit)
            serializer = self.serializer_class(elem, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class GetUniquePav(ListAPIView):
    serializer_class = serializers.PavSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self, pk):
        try:
            elem = models.Pav.objects.get(pk=pk)
        except models.Pav.DoesNotExist:
            return None
        return elem

    # Get a Pav
    def get(self, request, *args, **kwargs):
        if "pk" in kwargs:
            elem = self.get_queryset(kwargs["pk"])   # !!!!!!!!!!!!!! verif
            if not elem:
                content = {
                    'error': 'Not Found'
                }
                return Response(content, status=status.HTTP_404_NOT_FOUND)
            serializer = self.serializer_class(elem)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class GetPavsByCom_ComCom(ListAPIView):
    serializer_class = serializers.PavSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        try:
            elem = models.Pav.objects.all()
        except models.Pav.DoesNotExist:
            return None
        return elem

    def get(self, request, *args, **kwargs):
        if "type" in kwargs and "pk" in kwargs:
            elem = self.get_queryset()
            if not elem:
                content = {
                    'error': 'error get Pav'
                }
                return Response(content, status=status.HTTP_404_NOT_FOUND)

            elem_filter = None
            if kwargs["type"].lower() == "commune":
                elem_filter = elem.filter(commune__id=kwargs["pk"])
            elif kwargs["type"].lower() == "comcom":
                elem_filter = elem.filter(commune__communauteCommune_id=kwargs["pk"])
            else:
                content = {
                    'error': 'search only by Commune or ComCom'
                }
                return Response(content, status=status.HTTP_404_NOT_FOUND)

            serializer = self.serializer_class(elem, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class GetUserVoteByConteneur(ListAPIView):
    serializer_class = serializers.UserVoteSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self, pk):
        try:
            elem = models.Conteneur.objects.get(pk=pk).fillingLevel.uservotelevel_set.all()
        except models.Pav.DoesNotExist:
            return None
        return elem

    def get(self, request, *args, **kwargs):
        if "pk" in kwargs:
            elem = self.get_queryset(kwargs["pk"])
            if elem == None:
                content = {
                    'error': 'Not Found'
                }
                return Response(content, status=status.HTTP_404_NOT_FOUND)
            elif not elem:
                return Response({}, status=status.HTTP_404_NOT_FOUND)
            serializer = self.serializer_class(elem, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class GetIncivilites(ListAPIView):
    serializer_class = serializers.InciviliteSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    renderer_classes = [JSONRenderer]
    pagination_class = CustomPagination

    def get_queryset(self, pk):
        ret = None
        try:
            ret = models.Pav.objects.get(pk=pk).incivilite.all()
        except models.Pav.DoesNotExist:
            return None
        return ret

    # Get a Pav
    def get(self, request, *args, **kwargs):
        elem = self.get_queryset(kwargs["pkPav"])   # !!!!!!!!!!!!!! verif

        if elem:
            paginate_queryset = self.paginate_queryset(elem)
            serializer = self.serializer_class(paginate_queryset, many=True)
            return self.get_paginated_response(serializer.data)

        # Sinon, des datas sont retourné a false et null
        content = {
            'error': 'Not Found'
        }
        return Response(content, status=status.HTTP_404_NOT_FOUND)


class GetProductInfo(ListAPIView):
    serializer_class = serializers.ProductCodeSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    renderer_classes = [JSONRenderer]

    def get_queryset(self, pk):
        ret = None
        try:
            ret = models.ProductCode.objects.get(productCode=pk)
        except models.ProductCode.DoesNotExist:
            return None
        return ret

    # Get a Pav
    def get(self, request, *args, **kwargs):
        if "pk" in kwargs:
            elem = self.get_queryset(kwargs["pk"])

            if elem:
                serializer = self.serializer_class(elem)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                ret = requests.get(f"https://world.openfoodfacts.org/api/v0/product/{kwargs['pk']}.json")
                if ret.status_code == 200:
                    ret_dict = ret.json()
                    if "status" in ret_dict and ret_dict["status"] == 1:
                        if "product_name_fr" in ret_dict["product"] and "packaging" in ret_dict["product"]:
                            new_product = models.ProductCode()
                            new_product.productCode = kwargs["pk"]
                            new_product.productName = ret_dict["product"]["product_name_fr"]
                            new_product.packaging = ret_dict["product"]["packaging"]
                            new_product.save()

                            serializer = self.serializer_class(new_product)
                            return Response(serializer.data, status=status.HTTP_200_OK)

        # Sinon, des datas sont retourné a false et null
        content = {
            'error': 'Not Found'
        }
        return Response(content, status=status.HTTP_404_NOT_FOUND)


class GetConfigAppCitoyen(ListAPIView):
    serializer_class = serializers.ConfigAppCitoyenSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        configApp = models.ConfigMenuAppCitoyen.objects.all()
        return configApp

    def get(self, request, *args, **kwargs):
        configApp = self.get_queryset()
        paginate_queryset = self.paginate_queryset(configApp)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)


class GetEntreprise(ListAPIView):
    serializer_class = serializers.EntrepriseSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        entreprise = models.Entreprise.objects.all()
        return entreprise

    def get(self, request, *args, **kwargs):
        entreprise = self.get_queryset()
        paginate_queryset = self.paginate_queryset(entreprise)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)

class GetDemandeAjoutComSearch(ListAPIView):
    serializer_class = serializers.DemandeAjoutComSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        commu = models.DemandeAjoutCom.objects.all()
        return commu

    def search_by_ComCom(self, search,):
        elem = self.get_queryset().filter(name__icontains=search)
        return elem
  

    # GET a Commune
    def get(self, request, *args, **kwargs):      

        if "search" in request.GET.keys():
            elem = self.search_by_ComCom(request.GET["search"])
            serializer = self.serializer_class(elem, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            demJajoutCom = self.get_queryset()
            paginate_queryset = self.paginate_queryset(demJajoutCom)
            serializer = self.serializer_class(paginate_queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)

#  ____   ___  ____ _____
# |  _ \ / _ \/ ___|_   _|
# | |_) | | | \___ \ | |
# |  __/| |_| |___) || |
# |_|    \___/|____/ |_|

class PostPav(ListCreateAPIView):
    serializer_class = serializers.PavSerializerSet
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        pavs = models.Pav.objects.all()
        return pavs

    # Create a new Pav
    def post(self, request, *args, **kwargs):
        if "json" in request.data and len(request.data["json"]) > 2:
            print(request.data["json"])
            try:
                d = json.loads(request.data["json"])
            except:
                return Response({'error': 'Error json format'}, status=status.HTTP_400_BAD_REQUEST)
            conteneurs = []

            if not ("conteneurs" in d and "communeID" in d
                    and "name" in d and "position" in d and "address" in d and "valide" in d
                    and "state" in d and "dateLeveePav" in d and "heureLeveePav" in d and "appartenance_ext" in d):
                return Response({'error': 'Error in pav values'}, status=status.HTTP_400_BAD_REQUEST)

            new_pav = models.Pav()
            new_pav.name = d["name"]
            try:
                new_pav.position = GEOSGeometry(d["position"])
            except:
                return Response({'error': 'Error value position'}, status=status.HTTP_400_BAD_REQUEST)
            new_pav.address = d["address"]
            new_pav.valide = d["valide"].lower() in ['true', '1']
            new_pav.state = d["state"]
            new_pav.dateLeveePav = d["dateLeveePav"]
            new_pav.heureLeveePav = d["heureLeveePav"]

            if d["appartenance_ext"] != 0:
                try:
                    elem = models.Entreprise.objects.get(pk=int(d["appartenance_ext"]))
                    new_pav.appartenance_ext = elem
                except:
                    return Response({'error': 'Error appartenance_ext in PAV'}, status=status.HTTP_400_BAD_REQUEST)

            # new_pav.photo = d["photo"]
            new_pav.dateCreate = datetime.datetime.now()

            new_pav.save()

            try:
                elem = models.Commune.objects.get(pk=int(d["communeID"]))
                new_pav.commune = elem
            except:
                new_pav.delete()
                return Response({'error': 'Error communeID'}, status=status.HTTP_400_BAD_REQUEST)

            if "photoID" in d:
                try:
                    elem = models.UploadedFile.objects.get(pk=int(d["photoID"]))
                    new_pav.photo = elem
                except:
                    new_pav.delete()
                    return Response({'error': 'Error photoID'}, status=status.HTTP_400_BAD_REQUEST)
            new_pav.save()

            if "conteneurs" in d:
                for cont in d["conteneurs"]:
                    if not ("type" in cont and "type_emplacement" in cont
                            and "content" in cont and "color" in cont
                            and "apparence" in cont and "state" in cont
                            and "valide" in cont
                            and "dateLeveePassageCont" in cont and "heureLeveePassageCont" in cont
                            and "appartenance_ext" in cont and "accesPMR" in cont):
                        for elem in new_pav.conteneurs.all():
                            elem.delete()
                        new_pav.delete()
                        return Response({'error': 'Error in conteneurs values'}, status=status.HTTP_400_BAD_REQUEST)
                    new_conteneur = models.Conteneur()

                    new_conteneur.type = cont["type"]
                    new_conteneur.type_emplacement = cont["type_emplacement"]
                    new_conteneur.content = cont["content"]
                    new_conteneur.color = cont["color"]
                    new_conteneur.apparence = cont["apparence"]
                    new_conteneur.state = cont["state"]
                    # new_conteneur.FillingLevel = int(cont["fillingLevel"])
                    new_conteneur.valide = cont["valide"].lower() in ['true', '1']
                    new_conteneur.dateLeveePassageCont = cont["dateLeveePassageCont"]
                    new_conteneur.heureLeveePassageCont = cont["heureLeveePassageCont"]

                    if cont["appartenance_ext"] != 0:
                        try:
                            elem = models.Entreprise.objects.get(pk=int(cont["appartenance_ext"]))
                            new_conteneur.appartenance_ext = elem
                        except:
                            for elem in new_pav.conteneurs.all():
                                elem.delete()
                            new_pav.delete()
                            new_conteneur.delete()
                            return Response({'error': 'Error appartenance_ext in Conteneur'}, status=status.HTTP_400_BAD_REQUEST)

                    new_conteneur.accesPMR = cont["accesPMR"].lower() in ['true', '1']
                    new_conteneur.date = datetime.datetime.now()
                    new_conteneur.user = request.user

                    try:
                        new_conteneur.save()
                    except:
                        for elem in new_pav.conteneurs.all():
                            elem.delete()
                        new_pav.delete()
                        return Response({'error': 'Error Conteneur save'}, status=status.HTTP_400_BAD_REQUEST)
                    new_pav.conteneurs.add(new_conteneur.id)
                    conteneurs.append(new_conteneur.id)

            new_pav.save()
            content = {
                'id': new_pav.id
            }
            return Response(content, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class PostConteneur(ListCreateAPIView):
    serializer_class = serializers.ConteneurSerializerSet
    renderer_classes = [JSONRenderer]

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # Create a new Conteneur
    def post(self, request, *args, **kwargs):    
        if "json" in request.data and len(request.data["json"]) > 2:
            print(request.data["json"])

            try:
                datas = json.loads(request.data["json"])
            except:
                return Response({'error': 'Error json format'}, status=status.HTTP_400_BAD_REQUEST)
          

            if not("type" in datas and "type_emplacement" in datas \
                and "content" in datas and "color" in datas \
                and "apparence" in datas and "state" in datas \
                and "valide" in datas \
                and "id_pav" in datas \
                and "dateLeveePassageCont" in datas and "heureLeveePassageCont" in datas \
                and "appartenance_ext" in datas and "accesPMR" in datas):

                return Response({'error': 'Error fill field in container'}, status=status.HTTP_400_BAD_REQUEST)


            new_conteneur = models.Conteneur()
            new_conteneur.type = datas["type"]
            new_conteneur.type_emplacement = datas["type_emplacement"]
            new_conteneur.content = datas["content"]
            new_conteneur.color = datas["color"]
            new_conteneur.apparence = datas["apparence"]
            new_conteneur.state = datas["state"]
            new_conteneur.valide = datas["valide"].lower() in ['true', '1']
            new_conteneur.dateLeveePassageCont = datas["dateLeveePassageCont"]
            new_conteneur.heureLeveePassageCont = datas["heureLeveePassageCont"]

            if datas["appartenance_ext"] != 0:
                try:
                    elem = models.Entreprise.objects.get(pk=int(datas["appartenance_ext"]))
                    new_conteneur.appartenance_ext = elem
                except:
                    return Response({'error': 'Error appartenance_ext'}, status=status.HTTP_400_BAD_REQUEST)

            new_conteneur.accesPMR = datas["accesPMR"].lower() in ['true', '1']
            new_conteneur.user = request.user

            new_conteneur.save()
            content = {
                'id': new_conteneur.id
            }
            try:
                elem_pav = models.Pav.objects.get(pk=int(datas["id_pav"]))
                elem_pav.conteneurs.add(new_conteneur.id)
                elem_pav.save()
            except models.Pav.DoesNotExist:
                new_conteneur.delete()
                return Response({'error': 'Error in Pav ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
            except:
                new_conteneur.delete()
                return Response({'error': 'Error in Pav ID format'}, status=status.HTTP_400_BAD_REQUEST)

            return Response(content, status=status.HTTP_201_CREATED)

        return Response({'error': 'Error in conteneur values'}, status=status.HTTP_400_BAD_REQUEST)


class PostIncivilite(ListCreateAPIView):
    serializer_class = serializers.InciviliteSerializer
    renderer_classes = [JSONRenderer]

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # Create a new Pav
    def post(self, request, *args, **kwargs):
        if "id_pav" in request.data.keys() and "sauvage" in request.data.keys() and "abime" in request.data.keys() and\
            "encombrant" in request.data.keys() and "debordement" in request.data.keys() and "comment" in request.data.keys():
            datas = request.data

            new_incivilite = models.Incivilite()


            # new_incivilite.photo =
            new_incivilite.poubelleSauvage = datas["sauvage"]
            new_incivilite.pavAbime = datas["abime"]
            new_incivilite.encombrant = datas["encombrant"].lower() in ['true', '1']
            new_incivilite.debordement = datas["debordement"].lower() in ['true', '1']
            new_incivilite.comment = datas["comment"]
            # new_incivilite.votingGood = datas["vGood"]
            # new_incivilite.votingBad = datas["vBad"]

            new_incivilite.save()
            new_incivilite.users.add(request.user)
            new_incivilite.save()
            content = {
                'id': new_incivilite.id
            }
            try:
                elem_pav = models.Pav.objects.get(pk=int(datas["id_pav"]))
                elem_pav.incivilite.add(new_incivilite.id)
                elem_pav.save()
            except models.Pav.DoesNotExist:
                new_incivilite.delete()
                return Response({'error': 'Error in Pav ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
            except:
                new_incivilite.delete()
                return Response({'error': 'Error in Pav ID format'}, status=status.HTTP_400_BAD_REQUEST)

            return Response(content, status=status.HTTP_201_CREATED)
        return Response({'error': 'Error datas'}, status=status.HTTP_400_BAD_REQUEST)


class PostVoteIncivilite(ListCreateAPIView):
    serializer_class = serializers.InciviliteSerializer
    renderer_classes = [JSONRenderer]

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # Create a new Pav
    def post(self, request, *args, **kwargs):
        if "id_inci" in request.data.keys() and "type" in request.data.keys():
            datas = request.data

            try:
                elem_inci = models.Incivilite.objects.get(pk=int(datas["id_inci"]))

                if elem_inci.users.filter(id=request.user.id).count() > 0:
                    if elem_inci.users.first().id == request.user.id:
                        return Response({"error": "Le createur de l'incivilité ne peut pas voter"},
                                        status=status.HTTP_400_BAD_REQUEST)
                    return Response({"error": "Deja saisie"}, status=status.HTTP_400_BAD_REQUEST)

                val = datas["type"]
                if "up" == val:
                    elem_inci.votingGood = elem_inci.votingGood + 1
                elif "down" == val:
                    elem_inci.votingBad = elem_inci.votingBad + 1
                else:
                    return Response({'error': 'Error in type value'}, status=status.HTTP_400_BAD_REQUEST)
                elem_inci.users.add(request.user)
                elem_inci.save()
            except models.Incivilite.DoesNotExist:
                return Response({'error': 'Error in Incivilite ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response({'error': 'Error in Incivilite ID format'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(status=status.HTTP_200_OK)
        return Response({'error': 'Error datas'}, status=status.HTTP_400_BAD_REQUEST)


class PostUpdateFillingLevel(ListCreateAPIView):
    serializer_class = serializers.FillingLevelSerializer
    renderer_classes = [JSONRenderer]

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def add_vote(self, user, fillinglevel, level):
        # create an instance of 'MeetingArrival' object
        uservotelevel_obj = models.UserVoteLevel(fillingLevel=fillinglevel, user=user, level=level)
        uservotelevel_obj.save()

        countvote = 0
        totalvote = 0

        for vote in fillinglevel.uservotelevel_set.all():
            countvote += 1
            totalvote += vote.level

        fillinglevel.level = totalvote / countvote

        fillinglevel.save()

    # Create a new Pav
    def post(self, request, *args, **kwargs):
        if "id_cont" in request.data.keys():
            datas = request.data

            try:
                elem_conteneur = models.Conteneur.objects.get(pk=int(datas["id_cont"]))
            except models.Conteneur.DoesNotExist:
                return Response({'error': 'Error in Conteneur ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response({'error': 'Error in Conteneur ID format'}, status=status.HTTP_400_BAD_REQUEST)

            try:
                pav_pk = elem_conteneur.pav_set.first().pk
            except:
                return Response({'error': 'Pav not found for this Conteneur'}, status=status.HTTP_400_BAD_REQUEST)

            if "reset" in request.data.keys() or "force_level" in request.data.keys():
                if (hasattr(request.user, 'userstaff') and request.user.userstaff.pav.get(pk=pav_pk)) or request.user.is_staff:
                    success = False
                    # Que sa soit un reset ou un force_level, on reset le level
                    if "reset" in request.data.keys() or "force_level" in request.data.keys():
                        try:
                            for vote in elem_conteneur.fillingLevel.uservotelevel_set.all():
                                vote.delete()
                            elem_conteneur.fillingLevel.level = 0
                            elem_conteneur.fillingLevel.date = datetime.datetime.now()
                            elem_conteneur.fillingLevel.save()
                            success = True
                        except:
                            return Response({'error': 'Reset fail'}, status=status.HTTP_400_BAD_REQUEST)

                    if "force_level" in request.data.keys():
                        try:
                            level = int(datas["force_level"])
                        except:
                            return Response({'error': 'Error in level format'}, status=status.HTTP_400_BAD_REQUEST)

                        try:
                            self.add_vote(request.user, elem_conteneur.fillingLevel, level)
                            success = True
                        except:
                            return Response({'error': 'FillingLevel not found'}, status=status.HTTP_400_BAD_REQUEST)

                    if success:
                        return Response(status=status.HTTP_200_OK)
                else:
                    return Response({'error': 'Not authorized'}, status=status.HTTP_400_BAD_REQUEST)
            elif "level" in request.data.keys():
                try:
                    endtime = timezone.now()
                    starttime = endtime - timedelta(hours=settings.FILLINGLEVEL_VOTE_INTERVAL_HOURS)

                    if elem_conteneur.fillingLevel.uservotelevel_set.filter(Q(date__range=(starttime, endtime)), Q(user=request.user)).count() > 0:
                        return Response({"error": "Deja saisie"}, status=status.HTTP_400_BAD_REQUEST)

                    try:
                        level = int(datas["level"])
                    except:
                        return Response({'error': 'Error in level format'}, status=status.HTTP_400_BAD_REQUEST)

                    self.add_vote(request.user, elem_conteneur.fillingLevel, level)
                except:
                    return Response({'error': 'FillingLevel not found'}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response(status=status.HTTP_200_OK)

        return Response({'error': 'Error datas'}, status=status.HTTP_400_BAD_REQUEST)


def create_file(request):
    pic_upload = models.UploadedFile()
    pic_upload.file = request.data['file']
    pic_upload.user = request.user
    pic_upload.save()
    return pic_upload


class PostUpload(ListCreateAPIView):
    renderer_classes = [JSONRenderer]

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # Create a new Pav
    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            form = UploadFileForm(request.POST, request.data)
            if form.is_valid():
                pic_upload = create_file(request)
                if "id_pav" in request.data:
                    try:

                        elem = models.Pav.objects.get(pk=int(request.data["id_pav"]))
                        elem.photo = pic_upload
                        elem.save()

                    except models.Pav.DoesNotExist:
                        return Response({'error': 'Error in Pav ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
                    except:
                        return Response({'error': 'Error in Pav ID format'}, status=status.HTTP_400_BAD_REQUEST)
                elif "id_inci" in request.data:
                    try:
                        elem = models.Incivilite.objects.get(pk=int(request.data["id_inci"]))
                        elem.photo = pic_upload
                        elem.save()
                    except models.Incivilite.DoesNotExist:
                        return Response({'error': 'Error in Incivilite ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
                    except:
                        return Response({'error': 'Error in Incivilite ID format'}, status=status.HTTP_400_BAD_REQUEST)
                # else:
                #     return Response(status=status.HTTP_400_BAD_REQUEST)

                url_path = os.path.join(settings.MEDIA_URL, pic_upload.file.name)
                content = {
                    'id': pic_upload.id,
                    'url': url_path
                }
                return Response(content, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)




class PostSignalement(ListCreateAPIView):
    serializer_class = serializers.SignalementSerializer
    renderer_classes = [JSONRenderer]

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        if "id_pav" in request.data.keys() and "comm" in request.data.keys() and "type" in request.data.keys():
            datas = request.data

            new_signalement = models.Signalement()

            try:
                pav = models.Pav.objects.get(pk=int(request.data["id_pav"]))
            except models.Pav.DoesNotExist:
                return Response({'error': 'Error in Pav ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response({'error': 'Error in Pav ID format'}, status=status.HTTP_400_BAD_REQUEST)

            new_signalement.user = request.user
            new_signalement.date = datetime.datetime.now()
            new_signalement.type = datas["type"]
            new_signalement.pav = pav
            new_signalement.commentaire = datas["comm"]

            new_signalement.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response({'error': 'Error datas'}, status=status.HTTP_400_BAD_REQUEST)


class GetSignalementsByCom_ComCom(ListAPIView):
    serializer_class = serializers.SignalementSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def get_queryset(self):
        try:
            elem = models.Signalement.objects.all()
        except models.Signalement.DoesNotExist:
            return None
        return elem

    def get(self, request, *args, **kwargs):
        if "type" in kwargs and "pk" in kwargs:
            elem = self.get_queryset()
            if elem == None:
                content = {
                    'error': 'error get Signalement'
                }
                return Response(content, status=status.HTTP_404_NOT_FOUND)
            elif not elem:
                return Response({}, status=status.HTTP_404_NOT_FOUND)
            elem_filter = None
            if kwargs["type"].lower() == "commune":
                elem_filter = elem.filter(pav__commune_id=kwargs["pk"])
            elif kwargs["type"].lower() == "comcom":
                elem_filter = elem.filter(pav__commune__communauteCommune_id=kwargs["pk"])
            else:
                content = {
                    'error': 'search only by Commune or ComCom'
                }
                return Response(content, status=status.HTTP_404_NOT_FOUND)

            serializer = self.serializer_class(elem, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class PostDemandeAjoutCom(ListCreateAPIView):
    serializer_class = serializers.DemandeAjoutComSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    pagination_class = CustomPagination
    renderer_classes = [JSONRenderer]

    def post(self, request, *args, **kwargs):
        if "idCom" in request.data.keys() and "idComCom" in request.data.keys():
            datas = request.data

            new_DemandeCom = models.DemandeAjoutCom()

            try:
                com = models.Commune.objects.get(pk=int(request.data["idCom"]))
                comCom = models.CommunauteCommune.objects.get(pk=int(request.data["idComCom"]))
            except models.Pav.DoesNotExist:
                return Response({'error': 'Error in ID (not exist)'}, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response({'error': 'Error in Pav ID format'}, status=status.HTTP_400_BAD_REQUEST)

            new_DemandeCom.commune= com
            new_DemandeCom.communauteCommune= comCom
            new_DemandeCom.description=""
            

            new_DemandeCom.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response({'error': 'Error datas'}, status=status.HTTP_400_BAD_REQUEST)
