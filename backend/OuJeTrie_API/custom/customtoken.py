from django.contrib import admin
from django.contrib.gis.db import models

from knox.admin import AuthTokenAdmin as BaseAuthTokenAdmin
from knox.models import AuthToken

from rest_framework import serializers


#  __  __           _      _
# |  \/  | ___   __| | ___| |
# | |\/| |/ _ \ / _` |/ _ \ |
# | |  | | (_) | (_| |  __/ |
# |_|  |_|\___/ \__,_|\___|_|


class AuthTokenAdv(models.Model):
    token = models.OneToOneField(AuthToken, on_delete=models.CASCADE)
    platform = models.TextField(default="", verbose_name='Platform')
    date = models.DateTimeField(auto_now_add=True, blank=True, verbose_name='Date created')


#     _       _           _
#    / \   __| |_ __ ___ (_)_ __
#   / _ \ / _` | '_ ` _ \| | '_ \
#  / ___ \ (_| | | | | | | | | | |
# /_/   \_\__,_|_| |_| |_|_|_| |_|


# Define an inline admin descriptor for UserAdv model
# which acts a bit like a singleton
class AuthTokenAdvInline(admin.StackedInline):
    model = AuthTokenAdv
    can_delete = False
    verbose_name_plural = 'Paramètres avancés'


# Define a new User admin
class AuthTokenAdmin(BaseAuthTokenAdmin):
    inlines = (AuthTokenAdvInline,)


# Re-register UserAdmin
admin.site.unregister(AuthToken)
admin.site.register(AuthToken, AuthTokenAdmin)


#  ____            _       _ _
# / ___|  ___ _ __(_) __ _| (_)_______ _ __
# \___ \ / _ \ '__| |/ _` | | |_  / _ \ '__|
#  ___) |  __/ |  | | (_| | | |/ /  __/ |
# |____/ \___|_|  |_|\__,_|_|_/___\___|_|


class TokenAdvSerializer(serializers.HyperlinkedModelSerializer):
    created = serializers.CharField(source='token.created')
    expiry = serializers.CharField(source='token.expiry')

    class Meta:
        model = AuthTokenAdv
        fields = ['created', 'expiry', 'platform']
