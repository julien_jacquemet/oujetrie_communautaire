from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from OuJeTrie_API.models import Pav, CommunauteCommune

from ..adminwidget.VerboseManyToManyRawId import VerboseManyToManyRawIdWidget


#  __  __           _      _
# |  \/  | ___   __| | ___| |
# | |\/| |/ _ \ / _` |/ _ \ |
# | |  | | (_) | (_| |  __/ |
# |_|  |_|\___/ \__,_|\___|_|


class UserAdv(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    level = models.IntegerField(default=0, verbose_name='Level')
    preferredpav = models.ForeignKey(Pav, on_delete=models.DO_NOTHING, blank=True, null=True, verbose_name='PAV favori')
    nbscan = models.IntegerField(default=0, verbose_name='Nombre de scan')

    #staffcommune = models.ManyToManyField(Commune, blank=True)

    class Meta:
        permissions = [
        ]

class UserStaff(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    communauteCommune = models.ManyToManyField(CommunauteCommune, blank=True)

    class Meta:
        permissions = [
            ("staff_commune", "Peut visualisé les communes attribuées"),
        ]
#     _       _           _
#    / \   __| |_ __ ___ (_)_ __
#   / _ \ / _` | '_ ` _ \| | '_ \
#  / ___ \ (_| | | | | | | | | | |
# /_/   \_\__,_|_| |_| |_|_|_| |_|


# Define an inline admin descriptor for UserAdv model
# which acts a bit like a singleton
class UserAdvInline(admin.StackedInline):
    raw_id_fields = ['preferredpav']
    model = UserAdv
    can_delete = False
    verbose_name_plural = 'Paramètres avancés'


class UserStaffInline(admin.StackedInline):
    raw_id_fields = ['communauteCommune']
    model = UserStaff
    can_delete = False
    verbose_name_plural = 'Staff'

    def formfield_for_dbfield(self, db_field, **kwargs):
        # Si le formfield est un ManyToMany, on utilise un widget custom pour l'affichage
        if db_field.many_to_many and db_field.name in self.raw_id_fields:
            kwargs['widget'] = VerboseManyToManyRawIdWidget(db_field.remote_field, self.admin_site)
        else:
            return super().formfield_for_dbfield(db_field, **kwargs)
        kwargs.pop('request')
        return db_field.formfield(**kwargs)


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (UserAdvInline, UserStaffInline)




# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


#  ___       _ _      _   _
# |_ _|_ __ (_) |_   | | | |___  ___ _ __
#  | || '_ \| | __|  | | | / __|/ _ \ '__|
#  | || | | | | |_   | |_| \__ \  __/ |
# |___|_| |_|_|\__|   \___/|___/\___|_|


@receiver(post_save)
def init_useradv_oncreate(sender, instance, **kwargs):
    if sender is User:
        if kwargs["created"]:
            useradv_obj = UserAdv(user=instance, )
            useradv_obj.save()
