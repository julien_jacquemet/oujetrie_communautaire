from . import models

from django.contrib.auth.models import User
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework import serializers

from django.conf import settings

from datetime import timedelta
from django.utils import timezone

# -----------------------------------------
# Serializers define the API representation.

class UserSerializerLite(serializers.HyperlinkedModelSerializer):
    roles = serializers.SerializerMethodField()
    level = serializers.SerializerMethodField()
    nbscan = serializers.SerializerMethodField()

    def get_roles(self, obj):
        if hasattr(obj, 'groups'):
            groupList = []
            for g in obj.groups.all():
                groupList.append(g.name)
            return groupList
        return ""

    def get_level(self, obj):
        if hasattr(obj, 'useradv'):
            return obj.useradv.level
        return ""

    def get_nbscan(self, obj):
        if hasattr(obj, 'useradv'):
            return obj.useradv.nbscan
        return ""

    class Meta:
        model = User
        fields = ['username', 'is_staff', 'level', 'roles', 'nbscan']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    roles = serializers.SerializerMethodField()
    level = serializers.SerializerMethodField()
    nbscan = serializers.SerializerMethodField()
    perms = serializers.SerializerMethodField()

    def get_roles(self, obj):
        if hasattr(obj, 'groups'):
            groupList = []
            for g in obj.groups.all():
                groupList.append(g.name)
            return groupList
        return ""

    def get_level(self, obj):
        if hasattr(obj, 'useradv'):
            return obj.useradv.level
        return ""

    def get_nbscan(self, obj):
        if hasattr(obj, 'useradv'):
            return obj.useradv.nbscan
        return ""

    def get_perms(self, obj):
        perms = []

        if obj.is_superuser:
            perms.append("superuser")

        for user_perms in obj.user_permissions.all():
            perms.append(user_perms.codename)
        return perms

    class Meta:
        model = User
        fields = ['pk', 'username', 'email', 'first_name', 'last_name', 'is_staff', 'level', 'roles', 'nbscan', 'perms']


class EntrepriseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Entreprise
        fields = ["id", "name", "region", "departement", "desc", "listeActivity","d1_name","d1_type","d1_type_emplacement","d1_contenu","d1_contenant","d1_apparence","d1_description_gen","d1_description_tri"]


class DechetSerializer(serializers.ModelSerializer):
    entreprise = EntrepriseSerializer(read_only=True)
    class Meta:
        model = models.Dechet
        fields = ["id", "name", "type", "type_emplacement", "contenu", "contenant", "apparence",
                  "description_gen", "description_tri", "photo_tri", "motClesDechet", "communauteCommune", "entreprise"]
        # date et user ne sont pas traités


class CommunauteCommuneSerializer(serializers.ModelSerializer):
    dechet = DechetSerializer(read_only=True, many=True)
    class Meta:
        model = models.CommunauteCommune
        fields = ['id', 'name','region', 'departement', 'epci', 'contentYelloWaste',
                  'contentBlueWaste', 'contentGreenWaste', 'desc', 'listeActivity', 'dechet']


class CommuneSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Commune
        fields = ['id', 'name', 'siren', 'postalCode', 'position', 'dateLeveePoubelle',
                  'heureLeveePoubelle', 'contentYelloWaste', 'contentBlueWaste',
                  'contentGreenWaste', 'listeActivity']
        # communauteCommune et nbPaV ne sont pas traités



class InciviliteSerializer(serializers.HyperlinkedModelSerializer):
    photo = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    def get_photo(self, obj):
        if hasattr(obj, 'photo') and hasattr(obj.photo, 'file'):
            url_file = obj.photo.file.url
            return url_file
        return ""

    def get_user(self, obj):
        if hasattr(obj, 'users'):
            return UserSerializerLite(obj.users.first()).data
        return ""

    class Meta:
        model = models.Incivilite
        fields = ['id', 'user', 'photo', 'poubelleSauvage', 'pavAbime', 'comment',
                  'votingGood', 'votingBad', 'encombrant', 'debordement', 'date']
        # photo est une valeur 'calculée'


class FillingLevelSerializer(serializers.HyperlinkedModelSerializer):
    already = serializers.SerializerMethodField()
    lastupdate = serializers.SerializerMethodField()

    def get_already(self, obj):
        if self.context and "request" in self.context:
            if not self.context["request"].user.id:
                return "Pas authentifié"

            endtime = timezone.now()
            starttime = endtime - timedelta(hours=settings.FILLINGLEVEL_VOTE_INTERVAL_HOURS)

            if obj.uservotelevel_set.filter(date__range=(starttime, endtime)).count() > 0:
                return "True"
        return "False"

    def get_lastupdate(self, obj):
        last = obj.uservotelevel_set.last()
        if last:
            return serializers.DateTimeField().to_representation(last.date)
        return ""


    class Meta:
        model = models.FillingLevel
        fields = ['level', 'date', 'already', 'lastupdate']
        # users ne sont pas traités
        # already est une valeur 'calculée', et le champ n'est pas en BDD


class ConteneurSerializer(serializers.HyperlinkedModelSerializer):
    fillingLevel = FillingLevelSerializer(read_only=True)

    class Meta:
        model = models.Conteneur
        fields = ['id', 'type', 'content', 'color', 'state', 'fillingLevel', 'valide',
                  'dateLeveePassageCont', 'heureLeveePassageCont', 'date', 'type_emplacement', 'apparence', 'accesPMR' ]
        # user n'est pas traité


class ConteneurExportSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Conteneur
        fields = ['type', 'content', 'color', 'state', 'valide',
                  'dateLeveePassageCont', 'heureLeveePassageCont', 'type_emplacement', 'apparence', 'accesPMR']


class PavSerializer(GeoFeatureModelSerializer):
    conteneurs = ConteneurSerializer(read_only=True, many=True)
    commune = CommuneSerializer(read_only=True)
    incivilite = InciviliteSerializer(read_only=True, many=True)
    photo = serializers.SerializerMethodField()

    def get_photo(self, obj):
        if hasattr(obj, 'photo') and hasattr(obj.photo, 'file'):
            url_file = obj.photo.file.url
            return url_file
        return ""

    class Meta:
        model = models.Pav
        geo_field = "position"
        fields = ['id', 'name', 'position', 'address', 'commune', 'conteneurs',
                  'incivilite', 'valide', 'state', 'dateLeveePav',
                  'heureLeveePav', 'photo', 'date']
        # photo est une valeur 'calculée'

class PavExportSerializer(serializers.HyperlinkedModelSerializer):
    conteneurs = ConteneurExportSerializer(read_only=True, many=True)
    position = serializers.SerializerMethodField()
    valide = serializers.SerializerMethodField()
    commune = serializers.SerializerMethodField()
    communename = serializers.SerializerMethodField()
    photopath = serializers.SerializerMethodField()

    def get_position(self, obj):
        return str(obj.position)

    def get_valide(self, obj):
        return str(obj.valide)

    def get_commune(self, obj):
        if obj.commune:
            return str(obj.commune.pk)
        return "1"

    def get_communename(self, obj):
        if obj.commune:
            return str(obj.commune.name)
        return "INCONNU"

    def get_photopath(self, obj):
        if hasattr(obj, 'photo') and hasattr(obj.photo, 'file'):
            url_file = obj.photo.file.url
            return url_file
        return ""

    class Meta:
        model = models.Pav
        #geo_field = "position"
        fields = ['name', 'address', 'commune', 'communename', 'conteneurs', 'position',
                  'valide', 'state', 'dateLeveePav',
                  'heureLeveePav', 'photopath']
        # photo est une valeur 'calculée'


class PavSerializerSet(GeoFeatureModelSerializer):
    photo = serializers.SerializerMethodField()

    def get_photo(self, obj):
        if hasattr(obj, 'photo') and hasattr(obj.photo, 'file'):
            url_file = obj.photo.file.url
            return url_file
        return ""

    class Meta:
        model = models.Pav
        geo_field = "position"
        fields = ['name', 'position', 'address', 'valide', 'state',
                  'dateLeveePav', 'heureLeveePav', 'photo', 'date']
        # commune, conteneurs et incivilite ne sont pas traités
        # photo est une valeur 'calculée'


class ConteneurSerializerSet(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Conteneur
        fields = ["id", "type", "content", "color", "state", "fillingLevel",
                  "valide", "dateLeveePassageCont", "heureLeveePassageCont"]
        # date et user ne sont pas traités


class CommuneSearchSerializer(serializers.HyperlinkedModelSerializer):
    communauteCommune = CommunauteCommuneSerializer(read_only=True)
    distance = serializers.SerializerMethodField()

    def get_distance(self, obj):
        if hasattr(obj, 'distance'):
            return int(obj.distance.km)
        return 0

    class Meta:
        model = models.Commune
        fields = ['id', 'name', 'siren', 'postalCode', 'distance', 'communauteCommune','position', 'dateLeveePoubelle']
        # nbPaV, position, dateLeveePoubelle, heureLeveePoubelle,
        # contentYelloWaste, contentBlueWaste, contentGreenWaste
        # et listeActivity ne sont pas traités

        # distance est une valeur 'calculée', et le champ n'est pas en BDD


class PavNearSerializer(GeoFeatureModelSerializer):
    conteneurs = ConteneurSerializer(read_only=True, many=True)
    commune = CommuneSerializer(read_only=True)
    incivilite = InciviliteSerializer(read_only=True, many=True)

    photo = serializers.SerializerMethodField()

    def get_photo(self, obj):
        if hasattr(obj, 'photo') and hasattr(obj.photo, 'file') :
            url_file = obj.photo.file.url
            return url_file
        return ""

    distance = serializers.SerializerMethodField()

    def get_distance(self, obj):
        return int(obj.distance.km)

    class Meta:
        model = models.Pav
        geo_field = "position"
        fields = ['id', 'name', 'position', 'address', 'commune', 'conteneurs',
                  'incivilite', 'valide', 'state', 'dateLeveePav',
                  'heureLeveePav', 'photo', 'date', 'distance']
        # photo est une valeur 'calculée'
        # distance est une valeur 'calculée', et le champ n'est pas en BDD


class ProductCodeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.ProductCode
        fields = ['productCode', 'productName', 'packaging']


class SignalementSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    def get_user(self, obj):
        if hasattr(obj, 'user'):
            return obj.user.username
        return ""
    class Meta:
        model = models.Signalement
        fields = ['user', 'date', 'type', 'pav', 'etatTraitement', 'commentaire']


class UserVoteSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.SerializerMethodField()

    def get_user(self, obj):
        if hasattr(obj, 'user'):
            return obj.user.username
        return ""

    class Meta:
        model = models.UserVoteLevel
        fields = ['user', 'date', 'level']

class ConfigAppCitoyenSerializer(serializers.HyperlinkedModelSerializer):
    communauteCommune = CommunauteCommuneSerializer(read_only=True)
    class Meta:
        model = models.ConfigMenuAppCitoyen
        fields = ['nomMenu', 'affichage', 'communauteCommune']


class DemandeAjoutComSerializer(serializers.HyperlinkedModelSerializer):
    communauteCommune = CommunauteCommuneSerializer(read_only=True)
    commune = CommuneSerializer(read_only=True)
    class Meta:
        model = models.DemandeAjoutCom
        fields = ['communauteCommune', 'commune', 'description', 'position']
