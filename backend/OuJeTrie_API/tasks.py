from celery import shared_task
from django.core.management import call_command

@shared_task
def clearFillingLevelUsers():
    call_command("clear_FillingLevel_Users", "expired")
    print("Task clearFillingLevelUsers started")
