#!/bin/sh
if [ "$POSTGRES_DB" = "OuJeTrie" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $POSTGRES_HOST 5432; do
      sleep 0.1
    done

    echo "Databse is started"
fi

if [ $1 = "coreRest" ]
then

	python manage.py makemigrations
	python manage.py migrate django_cron
	python manage.py migrate
	python manage.py collectstatic --noinput

	if [ $2 = "init" ]
	then
		echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'pass')" | python manage.py shell
        	python manage.py shell < initCC.py
	fi
      	python manage.py email_confirm admin@example.com

	#python manage.py runserver 0.0.0.0:8000 --insecure

	gunicorn OuJeTrie.wsgi:application --bind 0.0.0.0:8000

fi

if [ $1 = "coreCelery" ]
then
	celery -A OuJeTrie worker -B -l info
fi

#exec "$@"
