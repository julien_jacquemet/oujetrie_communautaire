**Installation dependance python:**
pip install -r requirements.txt

**Installation ( dans manage.py )**
migrate
makemigrations
createsuperuser
admin
admin@example.com ( ou un autre email )
Password@1
Password@1
makemigrations
migrate

**Installation depuis docker**
faire un docker-compose up -d --build pour forcer la recreation

Attention, login et mdp dans entrypoint.sh

**Conf pycharm**
Désactivation de quelques warnings:
File --> Settings --> Editor --> Inspections:
Dans General, décoché "Duplicated code fragment"
Dans Proofreading, décoché "Typo"
File --> Settings --> Editor --> Code Style:
Modifier "Hard wrap at" a 175

**Exemple commande:**
register:
http POST http://127.0.0.1:8000/auth/registration/ username="UserTest" password1="Password@Test" password2="Password@Test" email="test@test.fr" platform="Android"
le champ platform est optionnel

login:
http POST http://127.0.0.1:8000/auth/login/ username=UserTest password=Password@Test platform=Android
le champ platform est optionnel

get All createDate + expireDate + platform
http GET http://127.0.0.1:8000/auth/list/ "Authorization: Token ICI_LE_TOKEN"

logout:
http POST http://127.0.0.1:8000/auth/logout/ "Authorization: Token ICI_LE_TOKEN"

logout all:
http POST http://127.0.0.1:8000/auth/logoutall/ "Authorization: Token ICI_LE_TOKEN"

get all pav:
http GET http://127.0.0.1:8000/api/v1/pavs/

get near pav:
limit et radius (en metres) sont optionnels
http GET "http://127.0.0.1:8000/api/v1/pavsn/?position=POINT(5+45)&limit=1&radius=1000"

get pav 1:
http http://127.0.0.1:8000/api/v1/pavs/1

get pav by commune id:
http http://127.0.0.1:8000/api/v1/pavs/commune/1

get pav by comcom id:
http http://127.0.0.1:8000/api/v1/pavs/comcom/1

get all conteneur
http http://127.0.0.1:8000/api/v1/conts/
http http://127.0.0.1:8000/api/v1/conts/ "Authorization: Token ICI_LE_TOKEN"

get conteneur by id
http http://127.0.0.1:8888/api/v1/conts/1

get uservote by conteneur ID:
http http://127.0.0.1:8000/api/v1/uservote/conteneur/1

post search communautecom
par nom
http GET "http://127.0.0.1:8000/api/v1/communautecom/search?search=Rhone"

get all commune
http http://127.0.0.1:8000/api/v1/commune/

post search commune
par nom
http GET "http://127.0.0.1:8000/api/v1/commune/search?search=Beaurepaire"

par pos
http GET "http://127.0.0.1:8000/api/v1/commune/search?position=POINT(5 45)"

par nom et position
http GET "http://127.0.0.1:8000/api/v1/commune/search?search=Beaurepaire&position=POINT(5 45)"

avec possibilité de choisir le nom de resultat max:
http GET "http://127.0.0.1:8000/api/v1/commune/search?search=Beaurepaire&position=POINT(5 45)&limit=5"

avec le nom et le code postal et la position et la limite:
http GET "http://127.0.0.1:8000/api/v1/commune/search?search=Beaurepaire&position=POINT(5.144841 45.364766)&postalcode=38260&limit=5"

avec la position et le code postal et la limite:
http GET "http://127.0.0.1:8000/api/v1/commune/search?position=POINT(5.144841 45.364766)&postalcode=38260&limit=5"

Ajout d'un nouveau conteneur lié a un Pav
http POST http://127.0.0.1:8000/api/v1/set/cont/ "Authorization: Token ICI_LE_TOKEN" type="1" content="2" color="3" state="4" fillingLevel="5" valide="True" dateLeveePassageCont="6" heureLeveePassageCont="7" id_pav=1 type_emplacement="AA" apparence="AA" appartenance_ext="1" accesPMR="true"

Ajout d'une anomalie
http POST http://127.0.0.1:8000/api/v1/add/anomalie/ "Authorization: Token ICI_LE_TOKEN" type="Le type" comm="Ceci est un comm" id_pav=1

get anomalie by commune id:
http http://127.0.0.1:8000/api/v1/anomalies/commune/1

get anomalie by comcom id:
http http://127.0.0.1:8000/api/v1/anomalies/comcom/1

Creation d'un PAV + 2 conteneur
http POST http://127.0.0.1:8000/api/v1/set/pavs/ "Authorization: Token ICI_LE_TOKEN" json="{\"name\":\"11\", \"position\":\"Point(5 23)\", \"address\":\"11\", \"valide\":\"11\", \"state\":\"11\", \"dateLeveePav\":\"11\", \"heureLeveePav\":\"11\", \"appartenance_ext\":\"1\", \"conteneurs\":[{\"type\":\"AA\", \"type_emplacement\":\"AA\", \"content\":\"AA\", \"color\":\"AA\", \"apparence\":\"AA\", \"state\":\"AA\", \"fillingLevel\":\"45\", \"valide\":\"true\", \"dateLeveePassageCont\":\"AA\", \"heureLeveePassageCont\":\"AA\", \"appartenance_ext\":\"1\", \"accesPMR\":\"true\"}, {\"type\":\"BB\", \"type_emplacement\":\"BB\", \"content\":\"BB\", \"color\":\"BB\", \"apparence\":\"BB\", \"state\":\"BB\", \"fillingLevel\":\"15\", \"valide\":\"true\", \"dateLeveePassageCont\":\"BB\", \"heureLeveePassageCont\":\"BB\", \"appartenance_ext\":\"1\", \"accesPMR\":\"false\"}], \"communeID\":1}"

Creation d'un PAV + 2 conteneur + ID photo
http POST http://127.0.0.1:8000/api/v1/set/pavs/ "Authorization: Token ICI_LE_TOKEN" json="{\"name\":\"11\", \"position\":\"Point(5 23)\", \"address\":\"11\", \"valide\":\"11\", \"state\":\"11\", \"dateLeveePav\":\"11\", \"heureLeveePav\":\"11\", \"appartenance_ext\":\"1\", \"conteneurs\":[{\"type\":\"AA\", \"type_emplacement\":\"AA\", \"content\":\"AA\", \"color\":\"AA\", \"apparence\":\"AA\", \"state\":\"AA\", \"fillingLevel\":\"45\", \"valide\":\"true\", \"dateLeveePassageCont\":\"AA\", \"heureLeveePassageCont\":\"AA\", \"appartenance_ext\":\"1\", \"accesPMR\":\"true\"}, {\"type\":\"BB\", \"type_emplacement\":\"BB\", \"content\":\"BB\", \"color\":\"BB\", \"apparence\":\"BB\", \"state\":\"BB\", \"fillingLevel\":\"15\", \"valide\":\"true\", \"dateLeveePassageCont\":\"BB\", \"heureLeveePassageCont\":\"BB\", \"appartenance_ext\":\"1\", \"accesPMR\":\"false\"}], \"communeID\":1, \"photoID\":1}"

json:
{
"name":"11",
"position":"Point(5 23)",
"address":"11",
"valide":"11",
"state":"11",
"dateLeveePav":"11",
"heureLeveePav":"11",
"appartenance_ext":"1",
"conteneurs":[
{
"type":"AA",
"type_emplacement":"AA",
"content":"AA",
"color":"AA",
"apparence":"AA",
"state":"AA",
"fillingLevel":"45",
"valide":"true",
"dateLeveePassageCont":"AA",
"heureLeveePassageCont":"AA",
"appartenance_ext":"1",
"accesPMR":"true"
},
{
"type":"BB",
"type_emplacement":"BB",
"content":"BB",
"color":"BB",
"apparence":"BB",
"state":"BB",
"fillingLevel":"15",
"valide":"true",
"dateLeveePassageCont":"BB",
"heureLeveePassageCont":"BB",
"appartenance_ext":"1",
"accesPMR":"false"
}
],
"communeID":1,
"photoID": 1
}

Ajout inciv
http POST http://127.0.0.1:8000/api/v1/add/inci/ "Authorization: Token ICI_LE_TOKEN" sauvage=False abime=False comment="3" id_pav=1

Get incivilite du Pav avec l'id 1
http http://127.0.0.1:8000/api/v1/incivilites/1

Exemple de retour (sous la meme forme que les listes de communes, liste de pav,...)
{
"count": 2,
"next": null,
"previous": null,
"results": [
{
"comment": "3",
"date": "2020-10-24T13:14:22.688664Z",
"id": 1,
"pavAbime": false,
"photo": "/media/uploadFiles/1_1603549705.jpg",
"poubelleSauvage": false,
"userID": {
"is_staff": true,
"username": "admin"
},
"votingBad": 0,
"votingGood": 0
},
{
"comment": "1212122121",
"date": "2020-10-30T21:34:23.485290Z",
"id": 2,
"pavAbime": false,
"photo": "/media/uploadFiles/1_1603549243.jpg",
"poubelleSauvage": true,
"userID": {
"is_staff": true,
"username": "admin"
},
"votingBad": 10,
"votingGood": 147
}
]
}

Up/Down sur une incivilité
http POST "http://127.0.0.1:8000/api/v1/vote/inci/" "Authorization: Token ICI_LE_TOKEN" id_inci="1" type="up"
http POST "http://127.0.0.1:8000/api/v1/vote/inci/" "Authorization: Token ICI_LE_TOKEN" id_inci=1 type="down"

upload de fichier:
curl -H "Authorization: Token ICI_LE_TOKEN" -F "file=@1.jpg" http://127.0.0.1:8000/api/v1/upload/
-F : pour specifié que c'est un "form"
file=@1.jpg : file est la key, et la value est le contenu de l'image nommé 1.jpg ( le @ permet a curl de recup le contenu du fichier)

upload de fichier dans un PAV existant:
curl -H "Authorization: Token ICI_LE_TOKEN" -F "id_pav=2" -F "file=@1.jpg" http://127.0.0.1:8000/api/v1/upload/

upload de fichier dans une incivilite existante:
curl -H "Authorization: Token ICI_LE_TOKEN" -F "id_inci=1" -F "file=@1.jpg" http://127.0.0.1:8000/api/v1/upload/

FillingLevel
http POST "http://127.0.0.1:8000/api/v1/update/fillinglevel/" "Authorization: Token ICI_LE_TOKEN" id_cont=1 level=50

FillingLevel staff - RESET (supprime tous les "votes")
http POST "http://127.0.0.1:8000/api/v1/update/fillinglevel/" "Authorization: Token ICI_LE_TOKEN" id_cont=1 reset=""

FillingLevel staff - FORCE_LEVEL ( supprime tous les "votes" et met un "vote")
http POST "http://127.0.0.1:8000/api/v1/update/fillinglevel/" "Authorization: Token ICI_LE_TOKEN" id_cont=1 force_level=50

GetProduct:
http GET "http://127.0.0.1:8000/api/v1/product/3270190121947"

{
"packaging": "valeur du champ packaging",
"productCode": "valeur du code bar",
"productName": "valeur du champ productName"
}

Entreprise (liste toutes les entreprises)
http GET "http://127.0.0.1:8000/api/v1/entreprise/"

ConfigAppCitoyen (liste toutes les config selon les comcom)
http GET "http://127.0.0.1:8000/api/v1/configappcitoyen/"

