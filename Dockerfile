FROM node:12.2.0-alpine as build_pwa
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
ENV NPM_CONFIG_UNSAFE_PERM=true
COPY package.json /app/package.json
RUN npm install --silent
RUN npm install @vue/cli@3.7.0 -g
COPY . /app
RUN npm run build

# production environment
FROM nginx:1.16.0-alpine
COPY --from=build_pwa /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 8099
CMD ["nginx", "-g", "daemon off;"]
