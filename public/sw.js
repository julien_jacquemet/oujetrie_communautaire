const CACHE_VERSION = '0.0.6';
const CACHE_NAME = 'version_build: '+CACHE_VERSION;
const CHANNEL_NOTIF="channel_notif_worker";
let dbReq;
let db;

var urlsToCache = [
  '/index.html',
   'index.js',
];

//Pour l'installation
self.addEventListener('install', function(event) {
  console.log('worker version ' + CACHE_NAME);
  sendMsg2Client({ version: CACHE_NAME });
  event.waitUntil( 
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');   
        cache.addAll(urlsToCache); 
      })
      .then(() => self.skipWaiting())
  );

}); 


//Pour actvier l'app
self.addEventListener('activate', function(event) {
  console.log('worker "activate" version ' + CACHE_NAME);
  
  var cacheWhitelist = [CACHE_NAME]; 

  event.waitUntil(
    //verifie si la DB est à upgrader
    createDB(),
    // Check de toutes les clés de cache.
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        //suppression du cache si nouvelles version
        cacheNames.map(function(cacheName) {
          if (cacheWhitelist.indexOf(cacheName) === -1) {
            console.log('Delete cache');
            caches.delete(cacheName);
          }
        })
      );
    })
    .then(() => self.clients.claim(), sendMsg2Client({ version: CACHE_NAME }), sendMsg2Client({ new_version: '' }))
  );

});

// fait une requete vers le serveur
const fromNetwork = (request, timeout) =>
  new Promise((fulfill, reject) => {
    const timeoutId = setTimeout(reject, timeout);
    fetch(request).then(response => {
      clearTimeout(timeoutId);
      fulfill(response);
      
      //on update le cache uniquement sur des requêtes GET
      if(request.method == "GET")
        update(request);

    }, reject).catch();
  });
  

// fait des requetes dans le cache
const fromCache = request =>
  caches
    .open(CACHE_NAME)
    .then(cache =>
      cache
        .match(request)
        .then(matching => matching || cache.match('/offline/'))
    );


// met les retours des demandes GET dans le cache
const update = request =>
  caches
    .open(CACHE_NAME)
    .then(cache =>
      fetch(request).then(response =>{
          if (request.method == "GET")           
            cache.put(request, response)          
        }
        )
    );


//Permet de stocker une requête si pas de réseau
async function StoreRequest(data) {   
  console.log("Sauvegarde une requête dans indexedDB")
  var obj = new Object();
  obj['url']=data.url;  
 
  //recupere les headers
  for (var entry of data.headers.entries()) {
    if( entry[0]=='authorization')
      obj['authorization']=entry[1];  
  }
  
  //recupère les formdata
  await data.formData().then(formData => {
    for(var pair of formData.entries()) {
      var key = pair[0];
      var value =  pair[1];
      obj[key] = value;     
    }    
  });

  storeRequestOnDb(obj);
}

//Permet de vider la DB si on retrouve le réseau
async function RefreshRequest()
{  
  var dataFromBDD=null; 
  
  //recupération de la première occurence
  await getRequestOnDb().then(function (response) {
    dataFromBDD=response;
  }).catch(function (error) {
    console.log(error.message);
  });

  if (dataFromBDD!=null)
  {
    console.log("Rejoue une requête");

    var url=dataFromBDD['url'];
    var auth=dataFromBDD['authorization'];

    //on garde uniquement les formData
    delete dataFromBDD['url'];
    delete dataFromBDD['authorization'];

    const bodyFormData = new URLSearchParams();

    for (var key in dataFromBDD) {
      if (dataFromBDD.hasOwnProperty(key)) {
        bodyFormData.append(key,dataFromBDD[key]);
      }
        
    }
    //on exécute la requete
    fetch(url, {
      method: "post",
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'authorization':auth
      },  
      
      body: bodyFormData
    })
    .then( (response) => { 
      console.log(response);
    });
  }

}


self.addEventListener('fetch', evt => {

  var fetchRequest = evt.request.clone();

  if (navigator.onLine === false) {    
    console.log("offline mode");
    var fetchRequestStore = evt.request.clone();

    //On sauvegarde la requete dans IndexedDB pour la rejouer plus tard
    StoreRequest(fetchRequestStore) ;
  }
  else{
    //on rejoue les requetes qui n'ont pas pu etre traité
    RefreshRequest()
  }

  if( evt.request.method == "GET")
  {    
    evt.respondWith(    
      fromNetwork(fetchRequest, 10000).catch(() => fromCache(fetchRequest))
    );
    evt.waitUntil(update(evt.request));
    
  }else if(evt.request.method == "POST")
  {
    evt.respondWith(    
      fromNetwork(fetchRequest, 10000)
    );
  }

  

});

self.addEventListener('push', function (event) {
  console.log('[Service Worker] Push Received.');
  console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
  

  if ("vibrate" in navigator) {
    navigator.vibrate(1000);
  }
});

//Permet de notifier l'app vue avec la version du site
function sendMsg2Client(oMsg) {
  // this uses the BroadCast Channel API. see https://developers.google.com/web/updates/2016/09/broadcastchannel
  setTimeout(() => {
    const oChannel = new BroadcastChannel(CHANNEL_NOTIF);
    oChannel.postMessage(oMsg);    
    oChannel.close();
  }, 10000);
}

//Creation de la database
function createDB() {
  
  dbReq = indexedDB.open('OuJeTriRequest', 1);
  dbReq.onupgradeneeded = function(event) {
    // Set the db variable to our database so we can use it!  
    db = event.target.result;

    // Create an object store named notes. Object stores
    // in databases are where data are stored.
    let notes = db.createObjectStore('requests', {autoIncrement: true});
  }
  dbReq.onsuccess = function(event) {
    db = event.target.result;
    console.log("Create Connexion OK");   
  }
  dbReq.onerror = function(event) {
    console.log('Error opening database ' + event.target.errorCode);
  }
    
  
}

//Stockage d'un json data dans la DB
function storeRequestOnDb(data) {
  var request = db.transaction(["requests"], "readwrite")
  .objectStore("requests")
  .add(data);
  
  request.onsuccess = function(event) {
     console.log("Requete enregistrée");
  };
  
  request.onerror = function(event) {
    console.log("Requete non enregistrée");
  }
}

//Recupère la première entrée en DB pour rejouer la requete
function getRequestOnDb()
{
  return new Promise(
    function(resolve, reject) {
      var request = db.transaction(["requests"], "readwrite");
      let store = request.objectStore('requests');
      
      let req = store.openCursor();
    
      req.onsuccess = function(event) {
        let cursor = event.target.result;
        if (cursor != null) {          
          var retData=cursor.value;
          cursor.delete();
          resolve(retData);     
        } else {   
          resolve(null);            
        }
      }
     
      req.onerror = function(event) {
        console.log('error getting note 1 ' + event.target.errorCode);
        reject(Error('Error text'));
      }
   }
  );
}


self.addEventListener('message', event => {});

function fetchWithParamAddedToRequestBody(request) {
  serialize(request).then(function(serialized) {
    deserialize(serialized).then(function(request) {
      return fetch(request);
    });
})};

function serialize(request) {
  var headers = {};
  for (var entry of request.headers.entries()) {
    headers[entry[0]] = entry[1];
  }
  var serialized = {
    url: request.url,
    headers: headers,
    method: request.method,
    mode: request.mode,
    credentials: request.credentials,
    cache: request.cache,
    redirect: request.redirect,
    referrer: request.referrer
  };  
  if (request.method !== 'GET' && request.method !== 'HEAD') {
    return request.clone().text().then(function(body) {
      serialized.body = body;
      return Promise.resolve(serialized);
    });
  }
  return Promise.resolve(serialized);
}
function deserialize(data) {
  console.log(data)
  return Promise.resolve(new Request(data.url, data));
}
